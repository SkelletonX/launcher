﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace Bussiness.Controls
{
    public class VerticalButton : Button
    {
        private float? _RPosition;
        private StringFormat _Format;
        public float? RPosition { get { return _RPosition; } set { _RPosition = value; Invalidate(); } }
        [Browsable(true)]
        public StringFormat Format { get { return _Format; } set { _Format = value; Invalidate(); } }

        protected override void OnHandleCreated(EventArgs e)
        {
            if (Format == null)
                Format = new StringFormat() { FormatFlags = StringFormatFlags.DirectionVertical };
            if (_RPosition == null)
                _RPosition = -1;

            base.OnHandleCreated(e);
        }

        protected override void OnCreateControl()
        {
            Format.Alignment = StringAlignment.Center;
            Format.LineAlignment = StringAlignment.Center;
            base.OnCreateControl();
        }

        protected override sealed void OnPaint(PaintEventArgs pevent)
        {
            Graphics G = pevent.Graphics;
            G.Clear(BackColor);
            G.TranslateTransform(Width / 2, Height / 2);
            G.RotateTransform(RPosition.Value);
            G.DrawString(Text, Font, new SolidBrush(ForeColor), new Rectangle(10, 10, Height, Width), Format);
            base.OnPaint(pevent);
        }

    }
}
