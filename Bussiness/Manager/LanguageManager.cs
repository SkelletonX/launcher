﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Bussiness.Info;
using Bussiness.Helper;
using System.Drawing;

namespace Bussiness.Manager
{
    public class LanguageManager
    {
        private static XmlDocument Document = null;
        private static LogHelper Log = LogHelper.GetLogger("LanguageManager");
        private static LanguageInfo[] _Languages = new LanguageInfo[0];
        public static LanguageInfo[] Languages { get { return _Languages; } }
        private static LanguageInfo _CurrentLanguage;
        public static LanguageInfo CurrentLanguage { get { return _CurrentLanguage; } }

        public static bool Init()
        {
            try
            {
                if (Document == null)
                    Document = new XmlDocument();
                Document.LoadXml(IOHelper.ReadText(StaticInfo.LanguageFile));
                List<LanguageInfo> TempLang = new List<LanguageInfo>();
                foreach (XmlNode Node in Document.SelectNodes("/LanguageManager/Language"))
                {
                    Image FlagImage = ImageHelper.GetOnlineImage(Node.Attributes["Icon"].Value);
                    Dictionary<string, string> Temp = new Dictionary<string, string>();
                    foreach (XmlNode StringNode in Node.SelectNodes("String"))
                    {
                        if (StringNode.Attributes["Name"] != null && !string.IsNullOrEmpty(StringNode.InnerText))
                            Temp.Add(StringNode.Attributes["Name"].Value, StringNode.InnerText);
                    }
                    LanguageInfo Info = new LanguageInfo
                        {
                            Name = Node.Attributes["Name"] != null ? Node.Attributes["Name"].Value : "Unknown_" + (new Random(99999).Next().To<string>()),
                            Flag = FlagImage,
                            Values = Temp,
                            Current = Node.Attributes["Current"] != null
                        };
                    if (Info.Current)
                        _CurrentLanguage = Info;
                    TempLang.Add(Info);
                }
                _Languages = TempLang.ToArray();
                return true;
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace);
                return false;
            }
        }

        public static bool ReLoad()
        {
            try
            {
                Array.Clear(Languages, 0, Languages.Length);
                return LanguageManager.Init();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace);
                return false;
            }
        }

        public static bool Save()
        {
            try
            {
                foreach (XmlNode Node in Document.SelectNodes("/LanguageManager/Language"))
                {
                    if (Node.Attributes["Current"] != null && Node.Attributes["Name"].Value != CurrentLanguage.Name)
                        Node.Attributes.Remove(Node.Attributes["Current"]);
                    else if (Node.Attributes["Current"] == null && Node.Attributes["Name"].Value == CurrentLanguage.Name)
                    {
                        XmlAttribute Attr = Document.CreateAttribute("Current");
                        Attr.Value = "true";
                        Node.Attributes.Append(Attr);
                    }
                }
                Document.Save(StaticInfo.LanguageFile);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace);
                return false;
            }
        }

        public static LanguageInfo GetLanguageByName(string Name)
        {
            foreach (LanguageInfo Language in Languages)
                if (Language.Name == Name)
                    return Language;
            return null;
        }

        public static void SetCurrentLanguage(LanguageInfo Language)
        {
            if (Language != null)
                _CurrentLanguage = Language;
        }
    }
}
