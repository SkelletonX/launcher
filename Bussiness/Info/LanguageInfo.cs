﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml;

namespace Bussiness.Info
{
    public class LanguageInfo
    {
        public string Name { get; set; }
        public Dictionary<string,string> Values { get; set; }
        public Image Flag { get; set; }
        public bool Current { get; set; }

        public LanguageInfo()
        {
            this.Name = "";
            this.Values = new Dictionary<string, string>();
            this.Flag = new Bitmap(16, 16);
            this.Current = false;
        }

        public string GetValueByName(string Name)
        {
            foreach (string Key in Values.Keys)
            {
                if (Key == Name)
                {
                    return Values[Key];
                }
            }
            throw new Exception("Invalid Name "+Name+"!");
        }
    }
}
