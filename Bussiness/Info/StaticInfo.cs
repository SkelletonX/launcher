﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Bussiness.Helper;
using System.Reflection;
using Bussiness.Manager;

namespace Bussiness.Info
{
    public class StaticInfo
    {
        public static readonly Encoding DefaultEncoding = Encoding.UTF8;
        #region WarRockPatch
        public static readonly double Version = 1.0; //Versão(necessária mudança caso atualize o arquivo do launcher) //
        public static readonly string FilesPath = IOHelper.CombinePaths(Environment.CurrentDirectory, "Files"); //Diretório do Download Manager
        public static readonly string ClientPath = Environment.CurrentDirectory;
        /*public static readonly string ClientPath = IOHelper.CombinePaths(IOHelper.GetDirectoryParent(Environment.CurrentDirectory), new string[1]
        {
        "Classic Duel"
        });*/
        public static readonly Dictionary<string, string> Links = new Dictionary<string, string>()
        {
            {"NewsLink","http://127.0.0.1/Launcher/"},
            {"WebLink","http://127.0.0.1/Launcher/"},
            {"RegisterLink","http://classicduel.com.br/index.php?do=cadastro"},
            {"ForumLink","http://forum.classicduel.com.br"},
            {"FacebookLink","https://www.facebook.com/ClassicNetwork?fref=ts"},
            {"TwitterLink","https://twitter.com/Classicftw"},
            {"ServerInfo","http://127.0.0.1/Launcher/users.php"}
        };
        public static readonly WebContentType ServerInfoContentType = WebContentType.POST;
        public static readonly string ServerInfoContent = "CG=1";
        public static readonly Dictionary<string, string> Images = new Dictionary<string, string>()
        {
            {"FacebookIcon","https://cdn4.iconfinder.com/data/icons/windev-contacts-2/512/facebook-16.png"},
            {"TwitterIcon","https://cdn2.iconfinder.com/data/icons/funky/64/Twitter-1-16.png"}
        };
        public static readonly IPEndPoint ServerEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 80);//IP:Port do Server
        public static readonly string GameClient = "Main.exe";//Executável do jogo
        public static readonly string GameName = "Nexus";//Nome do jogo
        public static readonly string UpdaterFile = "CLIENT_V_{0}.zip";//Arquivo que o Download Manager necessita (Necessário o {0}, ele pega a versão)
        //WARN:O Arquivo precisa ser feito por você, os arquivos do patcher/download e bussiness são necessários
        public static readonly Dictionary<string, string> LauncherFiles = new Dictionary<string, string>()
        { 
            {"Launcher", "Launcher.exe"}, 
            {"Downloader", "DownloadUpdater.exe" }
        };//Nome do arquivo do launcher e downloader(verifique os csproj)
        public static readonly string ClientArguments = "__rxstudios__service_original__";//Argumentos do executável do jogo caso necessário
        public static readonly string[] DeniedFiles = new string[] { "thumbs.db" };//Arquivos que não serão listados no Version.xml
        public static readonly bool ExitAfterStart = true;//Caso seja true, sai do launcher após iniciar o jogo
        public static readonly bool OnlyOneGameInstance = true;//Caso o jogo esteja aberto, o launcher não abre
        public static readonly bool AutoStart = true;//Inicia o launcher automaticamente após o Splash
        public static readonly byte[] TripleDesKey = new byte[24] { 0xFF, 0xAC, 0xAD, 0xB1, 20, 0xD2, 0xFA, 0xF2, 
                                                                    0xFF, 0x2A, 0x2D, 50, 20, 0xB1, 0xFF, 0xF3,
                                                                    0xFF, 0x2B, 0xD2, 0xB5, 0xAC, 0xDC, 0xAA, 0xDD };
        public static readonly byte[] TripleDesIV = { 8, 7, 6, 5, 4, 3, 2, 1 };
        #endregion
        public static readonly string[] SizeSuffixes = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };//Sufixos de tamanho(não necessário mudar)
        #region LanguageManager
        public static readonly string LanguageFile = IOHelper.CombinePaths(Environment.CurrentDirectory, "Strings.xml");//Arquivo do pacote de idiomas
        #endregion
        #region Log Helper
        public static bool LogWithDateTime = true;
        public static readonly string LogMessageFormat = "<{0}> [{1}]: ";
        public static readonly string LogFile = Path.Combine(Environment.CurrentDirectory, "CG-Launcher.log");
        public static readonly string LogDateFormat = "[{0}]";
        public static readonly ConsoleColor[] LogColors = new ConsoleColor[5]
        {
            ConsoleColor.White,//Info Color
            ConsoleColor.Red,//Error Color
            ConsoleColor.Yellow,//Warn Color
            ConsoleColor.White,//DateTime Color
            ConsoleColor.Green//Success Color
        };
        public static readonly string WarnMessage = "WARN";
        public static readonly string ErrorMessage = "ERROR";
        public static readonly string InfoMessage = "INFO";
        public static readonly string SuccessMessage = "SUCCESS";
        #endregion
        #region Console Helper
        public static readonly ConsoleColor IntroColor = ConsoleColor.Cyan;
        public static readonly ConsoleColor ForeColor = ConsoleColor.Gray;
        public static readonly ConsoleColor BackColor = ConsoleColor.Black;
        public static readonly ConsoleColor BoxColor = ConsoleColor.Green;
        public static readonly ConsoleColor TitleColor = ConsoleColor.White;
        #endregion
    }
}
