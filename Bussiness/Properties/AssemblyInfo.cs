﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Informações gerais sobre um assembly são controladas através do seguinte 
// conjunto de atributos. Altere o valor destes atributos para modificar a informação
// associada a um assembly.
[assembly: AssemblyTitle("Bussiness API")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright tDarkFall ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Definir ComVisible como false torna os tipos neste assembly não visíveis 
// para componentes COM. Caso precise acessar um tipo neste assembly a partir de 
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// O GUID a seguir é para o ID da typelib se este projeto for exposto para COM
[assembly: Guid("d811454e-07a0-4c0d-8ea5-2d8342a17f9c")]

// Informações de Versão para um assembly consistem nos quatro valores a seguir:
//
//      Versão Principal
//      Versão Secundária 
//      Número da Versão
//      Revisão
//
// É possível especificar todos os valores ou usar o padrão de Números de Compilação e Revisão 
// utilizando o '*' como mostrado abaixo:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
