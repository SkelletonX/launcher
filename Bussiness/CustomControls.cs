﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Security.Permissions;
using Bussiness.Helper;
using System.Drawing.Drawing2D;

namespace Bussiness.Controls
{
    public class FlagSelector : ComboBox
    {
        private Color _BorderColor;
        public Color BorderColor { get { return _BorderColor; } set { _BorderColor = value; Invalidate(); } }
        private ButtonBorderStyle _BorderStyle;
        public ButtonBorderStyle BorderStyle { get { return _BorderStyle; } set { _BorderStyle = value; Invalidate(); } }
        private static int WM_PAINT = 0x000F;
        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_PAINT)
            {
                Graphics g = CreateGraphics();
                Rectangle R = new Rectangle(0, 0, Width, Height);
                ControlPaint.DrawBorder(g, R, BorderColor, BorderStyle);
                g.DrawRectangle(new Pen(Brushes.Gray), ClientRectangle);
            }
        }


        // Draws the items into the ColorSelector object
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                e.DrawBackground();
                e.DrawFocusRectangle();

                DropDownItem item = (DropDownItem)Items[e.Index];
                // Draw the colored 16 x 16 square
                if(item.Image != null)
                e.Graphics.DrawImage(item.Image, e.Bounds.Left, e.Bounds.Top);
                // Draw the value (in this case, the color name)
                e.Graphics.DrawString(item.Value, e.Font, new
                        SolidBrush(e.ForeColor), e.Bounds.Left + item.Image.Width, e.Bounds.Top + 2);

                base.OnDrawItem(e);
            }
        }
    }

    public class DropDownItem
    {
        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }
        private string value;

        public Image Image
        {
            get { return img; }
            set { img = value; }
        }
        private Image img;

        public DropDownItem()
            : this("")
        { }

        public DropDownItem(string val, Image Icon)
        {
            value = val;
            this.img = Icon;
            Graphics g = Graphics.FromImage(Icon);
            //g.DrawRectangle(Pens.White, 0, 0, img.Width + 1, img.Height + 1);
            g.DrawImage(this.img, 0, 0, img.Width, img.Height);
        }

        public DropDownItem(string val)
        {
            value = val;
            this.img = new Bitmap(16, 16);
            Graphics g = Graphics.FromImage(img);
            Brush b = new SolidBrush(Color.FromName(val));
            g.DrawRectangle(Pens.White, 0, 0, img.Width, img.Height);
            g.FillRectangle(b, 1, 1, img.Width - 1, img.Height - 1);
        }

        public override string ToString()
        {
            return value;
        }
    }
}
