﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness.Info;
using System.IO;
using System.Security.Cryptography;

namespace Bussiness.Helper
{

    public static class CryptHelper
    {
        public static byte[] EncryptData(this byte[] Data)
        {
            TripleDES TD = new TripleDES(StaticInfo.TripleDesKey, StaticInfo.TripleDesIV);
            byte[] Temp = TD.Encrypt(Data);
            return Encoding.Default.GetBytes(Convert.ToBase64String(Temp));
        }

        public static byte[] DecryptData(this byte[] Data)
        {
            byte[] Temp = Convert.FromBase64String(Encoding.Default.GetString(Data));
            TripleDES TD = new TripleDES(StaticInfo.TripleDesKey, StaticInfo.TripleDesIV);
            return TD.Decrypt(Temp);
        }
    }
}
