﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Bussiness.Helper
{
    public enum WebContentType
    {
        POST,
        GET
    }

    public class WebHelper
    {
        private static object Locker = new object();

        public static bool IsOnline(IPEndPoint Host)
        {
            lock (Locker)
            {
                try
                {
                    TcpClient Client = new TcpClient();
                    Client.Connect(Host);
                    Client.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    DialogHelper.ShowError(ex.Message + Environment.NewLine + ex.StackTrace);
                    return false;
                }
            }
        }

        public static byte[] GetOnlineData(string URL) { return GetOnlineData(new Uri(URL)); }

        public static byte[] GetOnlineData(Uri URL)
        {
            lock (Locker)
            {
                try
                {
                    WebClient wc = new WebClient();
                    return wc.DownloadData(URL);
                }
                catch
                {
                    return new byte[byte.MinValue];
                }
            }
        }

        public static byte[] GetOnlineDataUpload(Uri URL, WebContentType ContentType = WebContentType.GET, string Content = "")
        {
            lock (Locker)
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.Headers.Add("Content-Type","application/x-www-form-urlencoded");
                    byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(Content);
                    return wc.UploadData(URL, ContentType == WebContentType.GET ? "GET" : "POST", byteArray);
                }
                catch
                {
                    return new byte[byte.MinValue];
                }
            }
        }
    }
}
