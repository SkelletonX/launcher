﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zip;
using System.IO;
using System.IO.Compression;

namespace Bussiness.Helper
{
    public static class StreamFunctions
    {
        public static long CopyTo(this Stream source, Stream destination)
        {
            byte[] buffer = new byte[2048];
            int bytesRead;
            long totalBytes = 0;
            while ((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0)
            {
                destination.Write(buffer, 0, bytesRead);
                totalBytes += bytesRead;
            }
            return totalBytes;
        }
    }

    public class Zip
    {
        public event EventHandler<ExtractProgressEventArgs> OnExtractProgress;

        private string File;
        public string Password { get; set; }
        public Zip(string File)
        {
            this.File = File;
        }

        protected virtual void ExtractProgress(ExtractProgressEventArgs e)
        {
            if (OnExtractProgress != null)
                OnExtractProgress(this, e);
        }

        public bool Compress(string[] Files)
        {
            try
            {
                ZipFile Zip = new ZipFile(this.File);
                Zip.ExtractProgress += new EventHandler<ExtractProgressEventArgs>((s, e) => ExtractProgress(e));
                foreach (string File in Files)
                {
                    Zip.AddFile(File);
                }
                if (this.Password != null && this.Password != "")
                    Zip.Password = this.Password;
                Zip.Save();
                return true;
            }
            catch (Exception ex)
            {
                DialogHelper.ShowError(ex.Message + " " + ex.StackTrace);
                return false;
            }
        }

        public bool UnCompress(string Path, bool WantOverwrite = false)
        {
            try
            {
                ZipFile Zip = new ZipFile(this.File);
                foreach (ZipEntry Entry in Zip.Entries)
                {
                    string RealFileName = IOHelper.CombinePaths(Path, Entry.FileName);
                    if (IOHelper.FileExists(RealFileName) && !WantOverwrite)
                        IOHelper.DeleteFile(RealFileName);
                    Entry.Extract(Path);
                }
                return true;
            }
            catch (Exception ex)
            {
                DialogHelper.ShowError(ex.Message + " " + ex.StackTrace);
                return false;
            }
        }

    }

    public class GZip
    {
        public static byte[] Compress(Stream input)
        {
            using (var compressStream = new MemoryStream())
            using (var compressor = new DeflateStream(compressStream, CompressionMode.Compress))
            {
                input.CopyTo(compressor);
                return compressStream.ToArray();
            }
        }

        public static Stream Decompress(byte[] input)
        {
            var output = new MemoryStream();

            using (var compressStream = new MemoryStream(input))
            using (var decompressor = new DeflateStream(compressStream, CompressionMode.Decompress))
                decompressor.CopyTo(output);

            output.Position = 0;
            return output;
        }
    }
}