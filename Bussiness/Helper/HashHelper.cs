﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Bussiness.Helper
{
    public class HashHelper
    {
        public static string Base64Encode(string Text, Encoding HashEncoding = null)
        {
            Encoding DefaultEncoding;
            if(HashEncoding == null)
                DefaultEncoding = Encoding.Default;
            else
                DefaultEncoding = HashEncoding;
            return Convert.ToBase64String(DefaultEncoding.GetBytes(Text));
        }

        public static string Base64Decode(string Text, Encoding HashEncoding = null)
        {
            Encoding DefaultEncoding;
            if (HashEncoding == null)
                DefaultEncoding = Encoding.Default;
            else
                DefaultEncoding = HashEncoding;
            return DefaultEncoding.GetString(Convert.FromBase64String(Text));
        }

        public static string GetFileMD5(string FPath)
        {
            string MDData;
            using (MD5 Hasher = MD5.Create())
            using (FileStream FS = File.OpenRead(FPath))
            {
                MDData = BitConverter.ToString(Hasher.ComputeHash(FS)).Replace("-", "").ToLower();
            }
            return MDData;
        }
    }

    public class TripleDES
    {
        private TripleDESCryptoServiceProvider m_des = new TripleDESCryptoServiceProvider();
        private UTF8Encoding m_utf8 = new UTF8Encoding();
        private byte[] m_key;
        private byte[] m_iv;

        public TripleDES(byte[] key, byte[] iv)
        {
            this.m_key = key;
            this.m_iv = iv;
        }

        public byte[] Encrypt(byte[] input)
        {
            return Transform(input,
                   m_des.CreateEncryptor(m_key, m_iv));
        }

        public byte[] Decrypt(byte[] input)
        {
            return Transform(input,
                   m_des.CreateDecryptor(m_key, m_iv));
        }

        public string Encrypt(string text)
        {
            byte[] input = m_utf8.GetBytes(text);
            byte[] output = Transform(input,
                            m_des.CreateEncryptor(m_key, m_iv));
            return Convert.ToBase64String(output);
        }

        public string Decrypt(string text)
        {
            byte[] input = Convert.FromBase64String(text);
            byte[] output = Transform(input,
                            m_des.CreateDecryptor(m_key, m_iv));
            return m_utf8.GetString(output);
        }

        private byte[] Transform(byte[] input,
                       ICryptoTransform CryptoTransform)
        {
            MemoryStream memStream = new MemoryStream();
            CryptoStream cryptStream = new CryptoStream(memStream,
                         CryptoTransform, CryptoStreamMode.Write);
            cryptStream.Write(input, 0, input.Length);
            cryptStream.FlushFinalBlock();
            memStream.Position = 0;
            byte[] result = memStream.ToArray();
            memStream.Close();
            cryptStream.Close();
            return result;
        }
    }
}
