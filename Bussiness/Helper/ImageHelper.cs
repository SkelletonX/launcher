﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Windows;

namespace Bussiness.Helper
{
    public static class ImageHelper
    {
        private static LogHelper Log = LogHelper.GetLogger("ImageHelper");
        public static Image GetOnlineImage(string URL)
        {
            Image FImage;
            try
            {
                if (IOHelper.FileExists(IOHelper.CombinePaths(Environment.CurrentDirectory, "Icons", IOHelper.GetFileName(URL))))
                    FImage = Image.FromFile(IOHelper.CombinePaths(Environment.CurrentDirectory, "Icons", IOHelper.GetFileName(URL)));
                else
                {
                    byte[] Buffer = WebHelper.GetOnlineData(URL);
                    Image BufferedImage = ImageHelper.StreamToImage(Buffer);
                    ImageHelper.SaveImage(BufferedImage, IOHelper.GetFileName(URL));
                    FImage = BufferedImage;
                }
            }
            catch (Exception IEx)
            {
                Log.Error(IEx.Message + " " + IEx.StackTrace);
                Bitmap TempBit = new Bitmap(16, 16);
                for (int X = 0; X < TempBit.Width; X++)
                    for (int Y = 0; Y < TempBit.Height; Y++)
                        TempBit.SetPixel(X, Y, Color.Black);
                FImage = TempBit;
            }
            return FImage;
        }

        public static Icon IconFromImage(this Image Image)
        {
            return Icon.FromHandle(((Bitmap)Image).GetHicon());
        }

        public static Image StreamToImage(byte[] Data)
        {
            Image Temp = null;
            try
            {
                using (MemoryStream Memory = new MemoryStream(Data))
                {
                    Temp = (new Bitmap(Memory) as Image);
                    Memory.Close();
                }
                return Temp;
            }
            catch
            {
                return null;
            }
        }

        public static bool SaveImage(Image File, string FileName)
        {
            try
            {
                string Path = IOHelper.CombinePaths(Environment.CurrentDirectory, "Icons");
                if (!Path.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()))
                    Path += System.IO.Path.DirectorySeparatorChar;
                if(!IOHelper.DirectoryExists(Path))
                    IOHelper.CreateDirectory(Path);
                File.Save(IOHelper.CombinePaths(Path, IOHelper.GetFileName(FileName)));
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " " + ex.StackTrace);
                return false;
            }
        }

        /// <summary>  
        /// method for changing the opacity of an image  
        /// </summary>  
        /// <param name="image">image to set opacity on</param>  
        /// <param name="opacity">percentage of opacity</param>  
        /// <returns></returns>  
        public static Image SetImageOpacity(this Image image, float opacity)
        {
            try
            {
                //create a Bitmap the size of the image provided  
                Bitmap bmp = new Bitmap(image.Width, image.Height);

                //create a graphics object from the image  
                using (Graphics gfx = Graphics.FromImage(bmp))
                {

                    //create a color matrix object  
                    ColorMatrix matrix = new ColorMatrix();

                    //set the opacity  
                    matrix.Matrix33 = opacity;

                    //create image attributes  
                    ImageAttributes attributes = new ImageAttributes();

                    //set the color(opacity) of the image  
                    attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                    //now draw the image  
                    gfx.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
                }
                return bmp;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return null;
            }
        } 
    }
}
