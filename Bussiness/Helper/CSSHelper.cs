﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bussiness.Helper
{
    public class CSSHelper
    {
        public static Dictionary<string, string> ToDictionary(HtmlElement Document, bool ToLower = false)
        {
            Dictionary<string, string> Style = new Dictionary<string, string>();
            string[] StyleData = Document.Style.Replace(": ", ":").Replace(" :", ":").Replace(";", Environment.NewLine).RegexSplit("[" + Environment.NewLine + "]");
            foreach (string Line in StyleData)
            {
                string TLine = Line;
                if (string.IsNullOrEmpty(TLine)) continue;
                if (TLine.Split(':').Length < 2) continue;
                if (TLine.StartsWith(" ")) TLine = TLine.Remove(0, TLine.LastIndexOf(' '));
                Style.Add(ToLower ? TLine.Split(':')[0].ToLower() : TLine.Split(':')[0],
                          ToLower ? TLine.Split(':')[1].ToLower() : TLine.Split(':')[1]);
            }
            return Style;
        }

        public static string ToString(Dictionary<string, string> StyleDictionary)
        {
            string Style = "";
            foreach (string StyleKey in StyleDictionary.Keys)
            {
                if (string.IsNullOrEmpty(StyleDictionary[StyleKey])) continue;
                Style += string.Format("{0}:{1};", StyleKey, StyleDictionary[StyleKey]) + Environment.NewLine;
            }
            return Style;
        }
    }

}
