﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.ComponentModel;
using System.Reflection;
using Bussiness.Info;
using System.Windows.Forms;

namespace Bussiness.Helper
{
    public static class ConversionHelper
    {
        public static string SizeSuffix(this Int64 value)
        {
            int mag = (int)Math.Log(value, 1024);
            int adjustedSize = (int)((decimal)value / (1 << (mag * 10)));
            if (adjustedSize.ToString().IndexOf(",") > -1)
                adjustedSize = int.Parse(adjustedSize.ToString().Substring(0, adjustedSize.ToString().IndexOf(",")));
            return string.Format("{0} {1}", adjustedSize, StaticInfo.SizeSuffixes[mag]);
        }

        public static T To<T>(this object Input)
        {
            return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(Input.ToString());
        }

        public static string PrintInfo(this object Input, BindingFlags Flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
        {
            string Data = Input.GetType().Name + " ( " + Environment.NewLine;
            Data += "Fields: " + Environment.NewLine + "(" + Environment.NewLine + string.Join(Environment.NewLine, Input.GetType().GetFields(Flags).Select(Field => Field.Name + ": (" + Field.GetType().ToString() + ")").ToArray()) + Environment.NewLine + ")" + Environment.NewLine;
            Data += "Properties: " + Environment.NewLine + "(" + Environment.NewLine + string.Join(Environment.NewLine, Input.GetType().GetProperties(Flags).Select(Propertie => Propertie.Name + ": (" + Propertie.GetType().ToString() + ")").ToArray()) + Environment.NewLine + ")" + Environment.NewLine;
            Data += "Methods: " + Environment.NewLine + "(" + Environment.NewLine + string.Join(Environment.NewLine, Input.GetType().GetMethods(Flags).Select(Method => Method.Name + ": (" + Method.GetType().ToString() + ", " + Method.GetParameters().Select(Parameter => Parameter.Name.ToString() + "|" + Parameter.DefaultValue.ToString()) + ")").ToArray()) + Environment.NewLine + ")" + Environment.NewLine;
            Data += "Members: " + Environment.NewLine + "(" + Environment.NewLine + string.Join(Environment.NewLine, Input.GetType().GetMembers(Flags).Select(Member => Member.Name + ": (" + Member.GetType().ToString() + ", " + Member.MemberType.ToString() + ")").ToArray()) + Environment.NewLine + ")" + Environment.NewLine;
            Data += "NestedTypes: " + Environment.NewLine + "(" + Environment.NewLine + string.Join(Environment.NewLine, Input.GetType().GetNestedTypes(Flags).Select(NestedType => NestedType.Name + ": (" + NestedType.Name + ", " + NestedType.GUID + ")").ToArray()) + Environment.NewLine + ")" + Environment.NewLine;
            return Environment.NewLine + Data + ") ";
        }

        public static bool ValidObject<T>(object Value, out T Return)
        {
            if (typeof(T) == typeof(int))
            {
                int Temp;
                bool Result = int.TryParse(Value.ToString(), out Temp);
                Return = (T)Convert.ChangeType(Temp, typeof(T));
                return Result;
            }
            else if (typeof(T) == typeof(double))
            {
                double Temp;
                bool Result = double.TryParse(Value.ToString(), out Temp);
                Return = (T)Convert.ChangeType(Temp, typeof(T));
                return Result;
            }
            else if (typeof(T) == typeof(decimal))
            {
                decimal Temp;
                bool Result = decimal.TryParse(Value.ToString(), out Temp);
                Return = (T)Convert.ChangeType(Temp, typeof(T));
                return Result;
            }
            else if (typeof(T) == typeof(float))
            {
                float Temp;
                bool Result = float.TryParse(Value.ToString(), out Temp);
                Return = (T)Convert.ChangeType(Temp, typeof(T));
                return Result;
            }
            else if (typeof(T) == typeof(short))
            {
                short Temp;
                bool Result = short.TryParse(Value.ToString(), out Temp);
                Return = (T)Convert.ChangeType(Temp, typeof(T));
                return Result;
            }
            else if (typeof(T) == typeof(IPAddress))
            {
                IPAddress Temp;
                bool Result = IPAddress.TryParse(Value.ToString(), out Temp);
                Return = (T)Convert.ChangeType(Temp, typeof(T));
                return Result;
            }
            else if (typeof(T) == typeof(string))
            {
                Return = (T)Convert.ChangeType(Value.ToString(), typeof(T));
                return !string.IsNullOrEmpty(Value.ToString());
            }
            else
            {
                try
                {
                    Return = (T)Convert.ChangeType(Value, typeof(T));
                    return true;
                }
                catch
                {
                    Return = default(T);
                    return false;
                }
            }
        }

        static void SafeCall<T>(Action<T> A, T C)
        {
            try
            {
                A(C);
            }
            catch(Exception ex)
            {
                throw ex.InnerException ?? ex;
            }
        }

        public static void Call<T>(this Control C, Action<T> Actn) where T : Control
        {
            if (C.InvokeRequired)
                C.Invoke(new Action(delegate { SafeCall(Actn, C as T); }));
            else
                Actn(C as T);
        }

        public static T2 Call<T, T2>(this Control C, Func<T, T2> Func) where T : Control
        {
            try
            {
                if (Func == null)
                    return default(T2);

                if (C.InvokeRequired)
                    return (T2)C.Invoke(Func, new object[] { C as T });
                else
                    return Func(C as T);
            }
            catch (Exception ex)
            {
                throw ex.InnerException ?? ex;
            }
        }

    }
}