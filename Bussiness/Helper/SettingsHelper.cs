﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Bussiness.Info;

namespace Bussiness.Helper
{
    public class SettingsHelper
    {
        public static Dictionary<string, string> LoadSettings()
        {
            Dictionary<string, string> TempDic = new Dictionary<string, string>();
            foreach (string Name in ConfigurationManager.AppSettings.Keys)
            {
                TempDic.Add(Name, ConfigurationManager.AppSettings[Name]);
            }
            return TempDic;
        }
    }
}
