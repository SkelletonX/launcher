﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Bussiness.Helper
{
    public static class StringHelper
    {
        public static IEnumerable<int> FindAllIndexes(this string str, string pattern)
        {
            int prevIndex = -pattern.Length; // so we start at index 0
            int index;
            while ((index = str.IndexOf(pattern, prevIndex + pattern.Length)) != -1)
            {
                prevIndex = index;
                yield return index;
            }
        }

        public static string ExtractString(this string Value, int Start, int End)
        {
            string Temp = "";
            for (int i = -1; i < Value.ToCharArray().Length; i++)
            {
                if (i > Start && i < End)
                    Temp = Temp + Value.ToCharArray()[i];
            }
            return Temp;
        }

        public static string Join(this object[] Input, string Separator = " ")
        {
            return string.Join(Separator, Input.Select(O => O.ToString()).ToArray());
        }

        public static string[] RegexSplit(this string Input,string Pattern)
        {
            return Regex.Split(Input, Pattern);
        }

        public static string FormatString(this string Input, params object[] Values)
        {
            return string.Format(Input, Values);
        }
    }
}
