﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bussiness.Helper
{
    public class DialogHelper
    {
        public static DialogResult ShowOpenDialog(out string FileName, string Text = "Open", string Filter = "All Files|*.*", bool MultiSelect = true)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Title = Text;
            OFD.Filter = Filter;
            DialogResult Result = OFD.ShowDialog();
            FileName = OFD.FileName;
            return Result;
        }

        public static DialogResult ShowOpenDialogMultiSelect(out string[] FileName, string Text = "Open", string Filter = "All Files|*.*", bool MultiSelect = true)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Title = Text;
            OFD.Filter = Filter;
            OFD.Multiselect = true;
            DialogResult Result = OFD.ShowDialog();
            FileName = OFD.FileNames;
            return Result;
        }

        public static DialogResult ShowSaveDialog(out string FileName, string Text = "Save", string Filter = "All Files|*.*")
        {
            SaveFileDialog SFD = new SaveFileDialog();
            SFD.Filter = Filter;
            SFD.Title = Text;
            DialogResult Result = SFD.ShowDialog();
            FileName = SFD.FileName;
            return Result;
        }

        public static DialogResult ShowMessage(string Message, string Text = "Message", MessageBoxButtons Buttons = MessageBoxButtons.OK, MessageBoxIcon Icon = MessageBoxIcon.Information)
        {
            return MessageBox.Show(Message, Text, Buttons, Icon);
        }

        public static DialogResult ShowError(string Message, string Text = "Error", MessageBoxButtons Buttons = MessageBoxButtons.OK, MessageBoxIcon Icon = MessageBoxIcon.Error)
        {
            return DialogHelper.ShowMessage(Message, Text, Buttons, Icon);
        }

        public static DialogResult ShowWarning(string Message, string Text = "Warning", MessageBoxButtons Buttons = MessageBoxButtons.OK, MessageBoxIcon Icon = MessageBoxIcon.Warning)
        {
            return DialogHelper.ShowMessage(Message, Text, Buttons, Icon);
        }

        public static DialogResult ShowExclamation(string Message, string Text = "Exclamation", MessageBoxButtons Buttons = MessageBoxButtons.OK, MessageBoxIcon Icon = MessageBoxIcon.Exclamation)
        {
            return DialogHelper.ShowMessage(Message, Text, Buttons, Icon);
        }
    }
}
