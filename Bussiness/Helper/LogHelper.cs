﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness;
using System.IO;
using System.Reflection;
using Bussiness.Info;

namespace Bussiness.Helper
{
    public class LogHelper
    {
        private static Dictionary<string, LogHelper> Logs = new Dictionary<string, LogHelper>();
        public string Name;
        public LogHelper(string Name)
        {
            this.Name = Name;
        }

        public LogHelper(MethodBase Method)
        {
            this.Name = Method.Name == ".cctor" || Method.Name == ".ctor" ? Method.GetType().Namespace : Method.Name;
        }

        public LogHelper(Type Class)
        {
            this.Name = Class.Name;
        }

        public static LogHelper GetLogger(string Name)
        {
            LogHelper Log;
            if (LogHelper.Logs.TryGetValue(Name, out Log))
                return Log;
            else
            {
                LogHelper.Logs.Add(Name, new LogHelper(Name));
                return GetLogger(Name);
            }
        }

        public static LogHelper GetLogger(MethodBase Method) { return GetLogger(Method.Name); }
        public static LogHelper GetLogger(Type Type) { return GetLogger(Type.Name); }

        private void WriteLogFile(string Message)
        {
            try
            {
                if (!File.Exists(StaticInfo.LogFile))
                    File.Create(StaticInfo.LogFile).Close();
                using (TextWriter Writer = File.AppendText(StaticInfo.LogFile))
                {
                    Writer.WriteLine(Message);
                    Writer.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Irá escrever no log uma info (Caso tenha um Console Instanciado, também ira escrever)
        /// </summary>
        /// <param name="Message">Mensagem a ser escrita</param>
        /// <param name="NewLine">Caso seja true, escrevera a mensagem e pulara linha, caso não só escrevera e continuara na mesma linha</param>
        /// <param name="LogMessageFormat">Format que a mensagem irá ser escrita (necessário 2 parametros)</param>
        public void Info(string Message, bool NewLine = true, string LogMessageFormat = null, bool WriteConsole = true)
        {
            string TempMessage = "";
            if (StaticInfo.LogWithDateTime)
            {
                TempMessage += string.Format(StaticInfo.LogDateFormat, DateTime.Now);
                if (WriteConsole)
                {
                    ConsoleColor C = Console.ForegroundColor;
                    Console.ForegroundColor = StaticInfo.LogColors[3];
                    Console.Write(StaticInfo.LogDateFormat, DateTime.Now);
                    Console.ForegroundColor = C;
                }
            }
            if (WriteConsole)
            {
                ConsoleColor CC = Console.ForegroundColor;
                Console.ForegroundColor = StaticInfo.LogColors[0];
                /*if (NewLine)
                    Console.WriteLine(LogMessageFormat ?? StaticInfo.LogMessageFormat, this.Name, Message);
                else
                    Console.Write(LogMessageFormat ?? StaticInfo.LogMessageFormat, this.Name, Message);*/
                Console.Write(LogMessageFormat ?? StaticInfo.LogMessageFormat, Name, StaticInfo.InfoMessage);
                Console.ForegroundColor = CC;
                if (NewLine)
                    Console.WriteLine(Message);
                else
                    Console.Write(Message);
            }
            TempMessage += string.Format(LogMessageFormat ?? StaticInfo.LogMessageFormat, this.Name, StaticInfo.InfoMessage) + Message;
            WriteLogFile(TempMessage);
        }

        /// <summary>
        /// Irá escrever no log um erro (Caso tenha um Console Instanciado, também ira escrever)
        /// </summary>
        /// <param name="Message">Mensagem a ser escrita</param>
        /// <param name="NewLine">Caso seja true, escrevera a mensagem e pulara linha, caso não só escrevera e continuara na mesma linha</param>
        /// <param name="LogMessageFormat">Format que a mensagem irá ser escrita (necessário 2 parametros)</param>
        public void Error(string Message, bool NewLine = true, string LogMessageFormat = null, bool WriteConsole = true)
        {
            string TempMessage = "";
            if (StaticInfo.LogWithDateTime)
            {
                TempMessage += string.Format(StaticInfo.LogDateFormat, DateTime.Now);
                if (WriteConsole)
                {
                    ConsoleColor C = Console.ForegroundColor;
                    Console.ForegroundColor = StaticInfo.LogColors[3];
                    Console.Write(StaticInfo.LogDateFormat, DateTime.Now);
                    Console.ForegroundColor = C;
                }
            }
            if (WriteConsole)
            {
                ConsoleColor CC = Console.ForegroundColor;
                Console.ForegroundColor = StaticInfo.LogColors[1];
                Console.Write(LogMessageFormat ?? StaticInfo.LogMessageFormat, Name, StaticInfo.ErrorMessage);
                Console.ForegroundColor = CC;
                if (NewLine)
                    Console.WriteLine(Message);
                else
                    Console.Write(Message);
            }
            TempMessage += string.Format(LogMessageFormat ?? StaticInfo.LogMessageFormat, this.Name, StaticInfo.ErrorMessage) + Message;
            WriteLogFile(TempMessage);
        }

        /// <summary>
        /// Irá escrever no log um aviso (Caso tenha um Console Instanciado, também ira escrever)
        /// </summary>
        /// <param name="Message">Mensagem a ser escrita</param>
        /// <param name="NewLine">Caso seja true, escrevera a mensagem e pulara linha, caso não só escrevera e continuara na mesma linha</param>
        /// <param name="LogMessageFormat">Format que a mensagem irá ser escrita (necessário 2 parametros)</param>
        public void Warn(string Message, bool NewLine = true, string LogMessageFormat = null, bool WriteConsole = true)
        {
            string TempMessage = "";
            if (StaticInfo.LogWithDateTime)
            {
                TempMessage += string.Format(StaticInfo.LogDateFormat, DateTime.Now);
                if (WriteConsole)
                {
                    ConsoleColor C = Console.ForegroundColor;
                    Console.ForegroundColor = StaticInfo.LogColors[3];
                    Console.Write(StaticInfo.LogDateFormat, DateTime.Now);
                    Console.ForegroundColor = C;
                }
            }
            if (WriteConsole)
            {
                ConsoleColor CC = Console.ForegroundColor;
                Console.ForegroundColor = StaticInfo.LogColors[2];
                Console.Write(LogMessageFormat ?? StaticInfo.LogMessageFormat, Name, StaticInfo.WarnMessage);
                Console.ForegroundColor = CC;
                if (NewLine)
                    Console.WriteLine(Message);
                else
                    Console.Write(Message);
            }
            TempMessage += string.Format(LogMessageFormat ?? StaticInfo.LogMessageFormat, this.Name, StaticInfo.WarnMessage) + Message;
            WriteLogFile(TempMessage);
        }
    }
}
