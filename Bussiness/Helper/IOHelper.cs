﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Bussiness.Info;

namespace Bussiness.Helper
{
    public class IOHelper
    {
        public static string[] GetFiles(string FPath) { return Directory.GetFiles(FPath, "*.*", SearchOption.AllDirectories); }
        public static string[] GetDirectories(string FPath) { return Directory.GetDirectories(FPath, "*.*", SearchOption.AllDirectories); }
        public static string GetFileName(string FPath) { return Path.GetFileName(FPath); }
        public static string GetFileNameWithoutExtension(string FPath)
        {
            string PathWithoutExtension = Path.ChangeExtension(Path.GetFileName(FPath), "");
            return PathWithoutExtension.EndsWith(".") ? PathWithoutExtension.Remove(PathWithoutExtension.Length - 1) : PathWithoutExtension;
        }
        public static string GetFileExtension(string FPath, bool WithDot = true) { return WithDot ? Path.GetFileName(FPath) : Path.GetFileName(FPath).Remove(0, 1); }
        public static string GetFileDirectory(string FPath) { return Path.GetDirectoryName(FPath); }
        public static string GetDirectoryParent(string FPath) { return Directory.GetParent(FPath).ToString(); }
        public static string[] ReadLines(string FPath) { return File.ReadAllLines(FPath, StaticInfo.DefaultEncoding); }
        public static byte[] ReadBytes(string FPath) { return File.ReadAllBytes(FPath); }
        public static string ReadText(string FPath) { return string.Join(Environment.NewLine, ReadLines(FPath)); }
        public static bool FileExists(string FPath) { return File.Exists(FPath); }
        public static bool DirectoryExists(string FPath) { return Directory.Exists(FPath); }
        public static FileStream CreateFile(string FPath) { return File.Create(FPath); }
        public static DirectoryInfo CreateDirectory(string FPath) { return Directory.CreateDirectory(FPath); }
        public static void DeleteFile(string FPath) { File.Delete(FPath); }
        public static void DeleteDirectory(string FPath) { Directory.Delete(FPath); }
        public static string GetTempPath() { return Path.GetTempPath(); }
        public static string GetTempFilename() { return Path.GetTempFileName(); }
        public static void SaveFile(string FPath, string Data, bool DeleteIfExists = true)
        {
            byte[] Buffer = StaticInfo.DefaultEncoding.GetBytes(Data);
            SaveFile(FPath, Buffer, DeleteIfExists);
        }
        public static void SaveFile(string FPath, byte[] Data, bool DeleteIfExists = true)
        {
            string FPath2;
            if (FPath.StartsWith("/"))
                FPath2 = FPath.Remove(0, 1);
            else
                FPath2 = FPath;
            if (DeleteIfExists && File.Exists(FPath2))
                File.Delete(FPath2);
            using (FileStream FStr = File.Open(FPath2, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                FStr.Write(Data, 0, Data.Length);
                FStr.Close();
            }
        }

        public static long GetFilesLength(params string[] Files)
        {
            long Total = 0;
            foreach (string FPath in Files)
            {
                Total += new FileInfo(FPath).Length;
            }
            return Total;
        }
        public static string CombinePaths(string path1, params string[] paths)
        {
            if (path1 == null)
            {
                throw new ArgumentNullException("path1");
            }
            if (paths == null)
            {
                throw new ArgumentNullException("paths");
            }
            String aggr = paths.Aggregate(path1, (acc, p) => Path.Combine(acc, p));
            if (IOHelper.GetFileExtension(aggr) == "" && !aggr.EndsWith(Path.DirectorySeparatorChar.ToString())) aggr += Path.DirectorySeparatorChar;
            return aggr;
        }
    }
}
