﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Bussiness.Helper
{
    public class CommandLineHelper
    {
        public static Dictionary<string, string> Parser(string[] Args = null)
        {
            string[] Arguments = Args == null ? Environment.GetCommandLineArgs() : Args;
            Regex R = new Regex(@"/(?<name>.+?):(?<val>.+)");
            Dictionary<string, string> Temp = new Dictionary<string, string>();
            foreach (string Argument in Arguments)
            {
                Match M = R.Match(Argument);
                if (M.Success)
                    Temp.Add(M.Groups[1].Value, M.Groups[2].Value);
            }
            return Temp;
        }
    }
}
