﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bussiness.Info;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace Bussiness.Helper
{
    public class ConsoleHelper
    {
        private static string[] Box = new string[] 
        { 
            "              ",//StartSpace
            " ",//Space
            "╓",//Left-Top
            "╖",//Right-Top
            "╚",//Left-Bottom
            "╝",//Right-Bottom
            "═",//Top-Bottom
            "║",//Left-Right
        };
        public static void DrawBox(string Title, string[] Messages, out int LineCount, int BoxWidth = 47)
        {
            //List<string> Temp = Messages.ToList();
            List<string> Temp2 = new List<string>();
            int LC = 0;
            foreach (string Temp in Messages)
            {
                if (Temp.Length > BoxWidth - 2)
                    Temp2.Add(Temp.Substring(BoxWidth - 2));
                else
                    Temp2.Add(Temp);
            }
            ConsoleColor B = Console.BackgroundColor;
            //ConsoleColor F = Console.ForegroundColor;
            Console.BackgroundColor = StaticInfo.BackColor;
            ConsoleColor Fore = StaticInfo.ForeColor;
            ConsoleColor Intro = StaticInfo.IntroColor;
            ConsoleColor BoxColor = StaticInfo.BoxColor;
            ConsoleColor TitleColor = StaticInfo.TitleColor;
            Console.ForegroundColor = BoxColor;
            Console.Clear();
            string Top = Box[0] + Box[2];
            for (int TopL = 0; TopL < BoxWidth; TopL++)
                Top = Top + Box[6];
            Top = Top + Box[3];
            Console.WriteLine(Top);
            LC++;
            Temp2.Insert(0, Title);
            for (int ID = 0; ID < Temp2.Count; ID++)
            {
                string Msg = Temp2[ID];
                Console.Write(Box[0] + Box[7]);
                while (Msg.Length < BoxWidth)
                    Msg = Box[1] + Msg + Box[1];
                if (Msg.Length == BoxWidth + 1)
                    Msg = Msg.Remove(Msg.Length - 1);
                Console.ForegroundColor = ID == 0 ? TitleColor : Intro;
                Console.Write(Msg);
                Console.ForegroundColor = BoxColor;
                Console.WriteLine(Box[7]);
                LC++;
            }
            string Bottom = Box[0] + Box[4];
            for (int BottomL = 0; BottomL < BoxWidth; BottomL++)
                Bottom = Bottom + Box[6];
            Bottom = Bottom + Box[5];
            Console.WriteLine(Bottom);
            LC++;
            //Console.BackgroundColor = B;
            Console.ForegroundColor = Fore;
            LineCount = LC;
        }

        public static void Center()
        {
            IntPtr hWin = GetConsoleWindow();
            RECT rc;
            GetWindowRect(hWin, out rc);
            Screen scr = Screen.FromPoint(new Point(rc.left, rc.top));
            int x = scr.WorkingArea.Left + (scr.WorkingArea.Width - (rc.right - rc.left)) / 2;
            int y = scr.WorkingArea.Top + (scr.WorkingArea.Height - (rc.bottom - rc.top)) / 2;
            MoveWindow(hWin, x, y, rc.right - rc.left, rc.bottom - rc.top, false);
        }

        // P/Invoke declarations
        private struct RECT { public int left, top, right, bottom; }
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetConsoleWindow();
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool GetWindowRect(IntPtr hWnd, out RECT rc);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int w, int h, bool repaint);
    }
}