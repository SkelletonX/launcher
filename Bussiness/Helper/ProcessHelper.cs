﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Management;

namespace Bussiness.Helper
{
    public class ProcessHelper
    {
        public static bool ProcessExists(string Name)
        {
            return Process.GetProcessesByName(Name).Length > 0;
        }

        public static bool ProcessExists(int PID)
        {
            return Process.GetProcessById(PID) != null;
        }

        public static Process[] GetProcess(string Name)
        {
            return Process.GetProcessesByName(Name);
        }

        public static Process GetProcess(int PID)
        {
            return Process.GetProcessById(PID);
        }

        public static double GetProcessMemoryByName(string Name)
        {
            return Math.Round(new PerformanceCounter("Process", "% Processor Time", Name).NextValue(), 1);
        }

        public static ulong GetCPUMemory()
        {
            return new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory;
        }

        public static string GetCPUUsage()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_PerfFormattedData_PerfOS_Processor");
            var cpuTimes = searcher.Get().Cast<ManagementObject>().Select(mo => new {Name = mo["Name"],Usage = mo["PercentProcessorTime"]}).ToList();
            var query = cpuTimes.Where(x => x.Name.ToString() == "_Total").Select(x => x.Usage);
            var cpuUsage = query.SingleOrDefault();
            return cpuUsage.ToString();
        }


    }
}
