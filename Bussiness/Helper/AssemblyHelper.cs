﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Bussiness.Helper
{
    public class AssemblyHelper
    {
        public static string GetAssemblyVersion(Assembly DLL)
        {
            Version V = DLL.GetName().Version;
            return V.ToString(4);
        }
        public static string GetAssemblyName(Assembly DLL)
        {
            return DLL.GetName().Name;
        }
    }
}
