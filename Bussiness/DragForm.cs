﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bussiness
{
    public class DragForm : Form
    {
        private bool Dragging { get; set; }
        private Point StartPoint { get; set; }
        public Point DragPoint { get; set; }

        protected override void OnCreateControl()
        {
            this.Dragging = false;
            this.DragPoint = new Point(Width, 10);
            base.OnCreateControl();
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.X <= DragPoint.X && e.Y <= DragPoint.Y)
            {
                this.Dragging = true;
                this.StartPoint = new Point(e.X, e.Y);
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            this.Dragging = false;
            base.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (this.Dragging)
            {
                Point ScreenPoint = PointToScreen(e.Location);
                this.Location = new Point(ScreenPoint.X - this.StartPoint.X, ScreenPoint.Y - this.StartPoint.Y);
            }
            base.OnMouseMove(e);
        }
    }
}
