﻿namespace DownloadUpdater
{
    partial class MainForm
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">verdade se for necessário descartar os recursos gerenciados; caso contrário, falso.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte do Designer - não modifique
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Theme = new Bussiness.Themes.GhostTheme.GhostTheme();
            this.LogoPictureBox = new System.Windows.Forms.PictureBox();
            this.ExitButton = new Bussiness.Themes.GhostTheme.GhostButton();
            this.DMGroupBox = new Bussiness.Themes.GhostTheme.GhostGroupBox();
            this.DownloadFileProgressBar = new Bussiness.Themes.GhostTheme.GhostProgressbar();
            this.Theme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
            this.DMGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Theme
            // 
            this.Theme.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Theme.Colors = new Bussiness.Themes.GhostTheme.Bloom[0];
            this.Theme.Controls.Add(this.LogoPictureBox);
            this.Theme.Controls.Add(this.ExitButton);
            this.Theme.Controls.Add(this.DMGroupBox);
            this.Theme.Customization = "";
            this.Theme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Theme.Font = new System.Drawing.Font("Verdana", 8F);
            this.Theme.Image = null;
            this.Theme.Location = new System.Drawing.Point(0, 0);
            this.Theme.Movable = true;
            this.Theme.Name = "Theme";
            this.Theme.NoRounding = false;
            this.Theme.ShowIcon = false;
            this.Theme.Sizable = true;
            this.Theme.Size = new System.Drawing.Size(455, 349);
            this.Theme.SmartBounds = true;
            this.Theme.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Theme.TabIndex = 0;
            this.Theme.Text = "Download Manager";
            this.Theme.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.Theme.Transparent = false;
            // 
            // LogoPictureBox
            // 
            this.LogoPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.LogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("LogoPictureBox.Image")));
            this.LogoPictureBox.Location = new System.Drawing.Point(11, 31);
            this.LogoPictureBox.Name = "LogoPictureBox";
            this.LogoPictureBox.Size = new System.Drawing.Size(433, 196);
            this.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPictureBox.TabIndex = 2;
            this.LogoPictureBox.TabStop = false;
            // 
            // ExitButton
            // 
            this.ExitButton.Color = System.Drawing.Color.Empty;
            this.ExitButton.Colors = new Bussiness.Themes.GhostTheme.Bloom[0];
            this.ExitButton.Customization = "";
            this.ExitButton.EnableGlass = true;
            this.ExitButton.Font = new System.Drawing.Font("Verdana", 8F);
            this.ExitButton.Image = null;
            this.ExitButton.Location = new System.Drawing.Point(11, 306);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.NoRounding = false;
            this.ExitButton.Size = new System.Drawing.Size(433, 31);
            this.ExitButton.TabIndex = 1;
            this.ExitButton.Text = "Cancel & Exit";
            this.ExitButton.Transparent = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // DMGroupBox
            // 
            this.DMGroupBox.Colors = new Bussiness.Themes.GhostTheme.Bloom[0];
            this.DMGroupBox.Controls.Add(this.DownloadFileProgressBar);
            this.DMGroupBox.Customization = "";
            this.DMGroupBox.Font = new System.Drawing.Font("Verdana", 8F);
            this.DMGroupBox.Image = null;
            this.DMGroupBox.Location = new System.Drawing.Point(11, 233);
            this.DMGroupBox.Name = "DMGroupBox";
            this.DMGroupBox.NoRounding = false;
            this.DMGroupBox.Size = new System.Drawing.Size(433, 67);
            this.DMGroupBox.TabIndex = 0;
            this.DMGroupBox.Text = "DMGroupBox";
            this.DMGroupBox.Transparent = false;
            // 
            // DownloadFileProgressBar
            // 
            this.DownloadFileProgressBar.Animated = true;
            this.DownloadFileProgressBar.Customization = "gICA/w==";
            this.DownloadFileProgressBar.Font = new System.Drawing.Font("Verdana", 8F);
            this.DownloadFileProgressBar.Image = null;
            this.DownloadFileProgressBar.Location = new System.Drawing.Point(3, 27);
            this.DownloadFileProgressBar.Maximum = 100;
            this.DownloadFileProgressBar.Name = "DownloadFileProgressBar";
            this.DownloadFileProgressBar.NoRounding = false;
            this.DownloadFileProgressBar.Size = new System.Drawing.Size(427, 34);
            this.DownloadFileProgressBar.TabIndex = 0;
            this.DownloadFileProgressBar.Transparent = false;
            this.DownloadFileProgressBar.Value = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 349);
            this.Controls.Add(this.Theme);
            this.DragPoint = new System.Drawing.Point(300, 5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WarRock Download Manager";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.Theme.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
            this.DMGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Bussiness.Themes.GhostTheme.GhostTheme Theme;
        private Bussiness.Themes.GhostTheme.GhostButton ExitButton;
        private Bussiness.Themes.GhostTheme.GhostGroupBox DMGroupBox;
        private System.Windows.Forms.PictureBox LogoPictureBox;
        private Bussiness.Themes.GhostTheme.GhostProgressbar DownloadFileProgressBar;

    }
}

