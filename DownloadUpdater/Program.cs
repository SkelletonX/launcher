﻿using Bussiness.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace DownloadUpdater
{
    static class Program
    {
        public static string[] Arguments { get; set; }
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main(string[] Args)
        {
            Program.Arguments = Args;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!MutexHelper.ThisProcessIsAlreadyRunning())
                Application.Run(new MainForm());
            else
                MutexHelper.SetFocusToPreviousInstance("MainForm");
        }
    }
}
