﻿using Bussiness;
using Bussiness.Helper;
using Bussiness.Info;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Bussiness.Manager;

namespace DownloadUpdater
{
    public partial class MainForm : DragForm
    {
        Dictionary<string, string> Arguments = new Dictionary<string,string>();
        private object Locker = new object();
        public MainForm()
        {
            if (!LanguageManager.Init())
            {
                DialogHelper.ShowMessage("Error on LanguageManager!!Verify String.xml and restart launcher!");
                Environment.Exit(Environment.ExitCode);
            }
            if (StaticInfo.OnlyOneGameInstance)
                if (ProcessHelper.ProcessExists(IOHelper.GetFileNameWithoutExtension(StaticInfo.GameClient)))
                {
                    DialogHelper.ShowWarning(LanguageManager.CurrentLanguage.GetValueByName("GameIsRunning").Replace("[BR]", Environment.NewLine));
                    Environment.Exit(Environment.ExitCode);
                }
            Arguments = CommandLineHelper.Parser();
            InitializeComponent();
            if (!Arguments.ContainsKey("v"))
            {
                DialogHelper.ShowWarning(LanguageManager.CurrentLanguage.GetValueByName("VersionArgumentWNotDeclared"), LanguageManager.CurrentLanguage.GetValueByName("VersionChecker"));
                Environment.Exit(Environment.ExitCode);
            }
            DMGroupBox.Text = LanguageManager.CurrentLanguage.GetValueByName("DownloadManager");
        }

        public void UpdateControls()
        {
            this.Text = StaticInfo.GameName + " - " + LanguageManager.CurrentLanguage.GetValueByName("DownloadManager");
            this.ExitButton.Text = LanguageManager.CurrentLanguage.GetValueByName("CancelAndExit");
        }

        protected override void OnLoad(EventArgs e)
        {
            lock (Locker)
            {
                try
                {
                    string FileName = IOHelper.CombinePaths(Environment.CurrentDirectory, "Updates", StaticInfo.UpdaterFile.FormatString(Arguments["v"]));
                    if (!IOHelper.DirectoryExists(IOHelper.GetFileDirectory(FileName)))
                        IOHelper.CreateDirectory(IOHelper.GetFileDirectory(FileName));
                    using (WebClient Work = new WebClient())
                    {
                        string RealURL = (StaticInfo.Links["WebLink"] + StaticInfo.UpdaterFile).FormatString(Arguments["v"]);
                        Work.DownloadProgressChanged += (s, eP) =>
                            {
                                DMGroupBox.Text = LanguageManager.CurrentLanguage.GetValueByName("DownloadManager")+" - ({0}):{1}%".FormatString(IOHelper.GetFileName(RealURL), eP.ProgressPercentage);
                                DownloadFileProgressBar.Value = eP.ProgressPercentage;
                            };
                        Work.DownloadDataCompleted += (s, eC) =>
                            {
                                if (eC.Cancelled)
                                {
                                    DialogHelper.ShowMessage(LanguageManager.CurrentLanguage.GetValueByName("DownloadCanceled").Replace("[BR]", Environment.NewLine));
                                    Environment.Exit(Environment.ExitCode);
                                }
                                else
                                {
                                    IOHelper.SaveFile(FileName, eC.Result);
                                    Zip File = new Zip(FileName);
                                    string FilesPath = IOHelper.CombinePaths(Environment.CurrentDirectory, "Updates", "Files");
                                    if (!IOHelper.DirectoryExists(IOHelper.GetFileDirectory(FilesPath)))
                                        IOHelper.CreateDirectory(IOHelper.GetFileDirectory(FilesPath));
                                    if (!File.UnCompress(FilesPath))
                                    {
                                        DialogHelper.ShowError("Error on File Uncompress!");
                                    }
                                    else
                                    {
                                        string Args = "/c \"";
                                        Args += "title {0} & ".FormatString(StaticInfo.GameName+" - "+LanguageManager.CurrentLanguage.GetValueByName("ExtractorManager"));
                                        Args += "color 0F & ";
                                        Args += "echo off & ";
                                        Args += "cls & ";
                                        Args += "echo {0} & ".FormatString(LanguageManager.CurrentLanguage.GetValueByName("WaitDownloadManagerExit"));
                                        Args += "timeout /T 5 /NOBREAK & ";
                                        Args += "xcopy \"./Updates/Files/*.*\" \"%cd%\" /i /e /f /y & ";
                                        Args += "rmdir /S /Q \"./Updates\" & ";
                                        Args += "start {0} & ".FormatString(StaticInfo.LauncherFiles["Launcher"]);
                                        Args += "pause";
                                        Args += "\"";
                                        Process.Start(new ProcessStartInfo
                                        {
                                            WorkingDirectory = Environment.CurrentDirectory,
                                            FileName = "cmd.exe",
                                            Arguments = Args,
                                            
                                        });
                                        Environment.Exit(Environment.ExitCode);
                                    }
                                }
                            };
                        Work.DownloadDataAsync(new Uri(RealURL));
                    }
                }
                catch (WebException WE)
                {
                    string Message = LanguageManager.CurrentLanguage.GetValueByName("DefaultError");
                    if (WE.Response != null && WE.Response is HttpWebResponse)
                    {
                        HttpWebResponse Response = WE.Response as HttpWebResponse;
                        switch (Response.StatusCode)
                        {
                            case HttpStatusCode.NotFound: Message = LanguageManager.CurrentLanguage.GetValueByName("Web404").Replace("[BR]", Environment.NewLine); break;
                            case HttpStatusCode.Forbidden: Message = LanguageManager.CurrentLanguage.GetValueByName("Web403").Replace("[BR]", Environment.NewLine); break;
                            default: Message += Environment.NewLine + LanguageManager.CurrentLanguage.GetValueByName("WebException").FormatString(WE.ToString()); break;
                        }
                    }
                    DialogHelper.ShowError(Message);
                    Environment.Exit(Environment.ExitCode);
                }
                catch (IOException)
                {
                    DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("IOError").Replace("[BR]", Environment.NewLine));
                    Environment.Exit(Environment.ExitCode);
                }
                catch (Exception E)
                {
                    DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("DefaultException").FormatString(E.Message + " " + E.StackTrace).Replace("[BR]", Environment.NewLine));
                    Environment.Exit(Environment.ExitCode);
                }
            }
            base.OnLoad(e); //ok
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            if (DialogHelper.ShowMessage(LanguageManager.CurrentLanguage.GetValueByName("ExitMessage"), LanguageManager.CurrentLanguage.GetValueByName("ExitTitle"), MessageBoxButtons.YesNo) == DialogResult.Yes)
                Environment.Exit(Environment.ExitCode);
        }
    }
}
