﻿using Bussiness.Helper;
using Bussiness.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.IO;

namespace MakeVersionXML
{
    class Program
    {
        static LogHelper Log = LogHelper.GetLogger(StaticInfo.GameName + " VersionXML Maker");
        static void Main(string[] args)
        {
            Console.Title = StaticInfo.GameName+" VersionXML Maker";
            int LC;
            //ConsoleHelper.Center();
            ConsoleHelper.DrawBox(StaticInfo.GameName+" VersionXML Maker", new string[]
            {
                "Version: {0}".FormatString(AssemblyHelper.GetAssemblyVersion(Assembly.GetEntryAssembly())),
                "Developed by: tDarkFall"
            }, out LC, 50);
            if (!IOHelper.DirectoryExists(StaticInfo.FilesPath))
            {
                IOHelper.CreateDirectory(StaticInfo.FilesPath);
                Log.Info("Drop your files in ./Files directory and try!");
                Console.ReadLine();
                Environment.Exit(Environment.ExitCode);
            }
            Console.WriteLine("Press enter then your ready!");
            Console.ReadLine();
            StartXMLMaker();
        }

        static void StartXMLMaker()
        {
            try
            {
                string[] Files = IOHelper.GetFiles(StaticInfo.FilesPath);
                if (IOHelper.FileExists(IOHelper.CombinePaths(Environment.CurrentDirectory, "Version.xml")))
                    IOHelper.DeleteFile(IOHelper.CombinePaths(Environment.CurrentDirectory, "Version.xml"));
                XmlDocument DOC = new XmlDocument();
                DOC.AppendChild(DOC.CreateXmlDeclaration("1.0", "UTF-8", "yes"));
                XmlElement Node = DOC.CreateElement("root");
                XmlAttribute Attr1 = DOC.CreateAttribute("Version");
                Attr1.Value = StaticInfo.Version.ToString().Replace(",", ".");
                Node.Attributes.Append(Attr1);
                foreach (string File in Files)
                {
                    if (Array.IndexOf(StaticInfo.DeniedFiles, IOHelper.GetFileName(File).ToLower()) > -1)
                        continue;
                    XmlNode ChildNode = DOC.CreateNode(XmlNodeType.Element, "File", null);
                    XmlAttribute Attr = DOC.CreateAttribute("MD5");
                    Attr.Value = HashHelper.GetFileMD5(File);
                    ChildNode.InnerText = File.Replace(StaticInfo.FilesPath, "").Remove(0, 1);
                    ChildNode.Attributes.Append(Attr);
                    Node.AppendChild(ChildNode);
                }
                DOC.AppendChild(Node);
                //DOC.Save(IOHelper.CombinePaths(Environment.CurrentDirectory, "Version.xml"));
                byte[] Data;
                using (MemoryStream Memory = new MemoryStream())
                {
                    DOC.Save(Memory);
                    Data = Memory.ToArray().EncryptData();
                    Memory.Close();
                }
                IOHelper.SaveFile(IOHelper.CombinePaths(Environment.CurrentDirectory, "Version.xml"), Data);
                Log.Info("End of XML Maker!!");
                Log.Info("Copy Version.xml and Files directory to web folder!!");
            }
            catch (Exception ex)
            {
                Log.Error("Error on XML Maker!! Verify .log file!");
                Log.Error(ex.Message + " " + ex.StackTrace, true, null, false);
            }
            Console.ReadLine();
        }
    }
}
