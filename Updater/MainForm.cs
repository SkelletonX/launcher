﻿using Bussiness;
using Bussiness.Helper;
using Bussiness.Info;
using Launcher.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Bussiness.Manager;
using System.Threading;
using Bussiness.Themes;
using System.Runtime.InteropServices;

namespace Launcher
{
    public partial class MainForm : Form
    {
        public bool Dragging { get; set; }
        public bool StartClicked { get; set; }
        public bool Updated { get { return Files.Count == 0; } }
        public Point StartPoint { get; set; }
        public string CurrentLabel { get; set; }
        public int Updates { get { return _Updates; } }
        public List<string> Files;

        
        private object Locker = new object();
        private Thread DownloadThread;
        private System.Threading.Timer ServerInfoTimer;
        private int _UpdateCount, _Updates;
        
        public MainForm(List<string> Files)
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            InitializeComponent();
            this.FlagIcon.Cursor = Cursors.Hand;
            this.Files = Files;
            this._Updates = this.Files.Count;
            this.StartClicked = false;
            this.CurrentLabel = "CheckingFiles";
            this.Text = "{0} - {1}: {2} | {3}".FormatString(StaticInfo.GameName, LanguageManager.CurrentLanguage.GetValueByName("Version"), StaticInfo.Version, " RX Studios").Replace(",", ".");
            this.FormTheme.Text = this.Text;
            this.NewsBrowser.Navigate(StaticInfo.Links["NewsLink"]);
            this.DownloadThread = new Thread(new ThreadStart(DoUpdate));
            this.FacebookIcon.Image = ImageHelper.GetOnlineImage(StaticInfo.Images["FacebookIcon"]);
            this.TwitterIcon.Image = ImageHelper.GetOnlineImage(StaticInfo.Images["TwitterIcon"]);
            this.ServerInfoLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("ServerInfo").FormatString(0);
            this.UpdateControls();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.ServerInfoTimer = new System.Threading.Timer(ServerInfoCallBack, null, 2000, Timeout.Infinite);
            this.ServerInfoTimer.Change(2000, Timeout.Infinite);
            this.FormTheme.GradientColors = new Color[] { Color.DarkRed, Color.FromArgb(128,0,0) };
            this.Focus();
            this.BringToFront();
        }

        private void ServerInfoCallBack(object s)
        {
            Uri InfoURL = new Uri(StaticInfo.Links["ServerInfo"]);
            byte[] Info = WebHelper.GetOnlineDataUpload(InfoURL, StaticInfo.ServerInfoContentType, StaticInfo.ServerInfoContent);
            string Count;
            if (Info == new byte[byte.MinValue]) Count = "0";
            else Count = Encoding.UTF8.GetString(Info);
            if (Count == string.Empty) Count = "0";
            this.ServerInfoLabel.Call<Label>(SIL => SIL.Text = LanguageManager.CurrentLanguage.GetValueByName("ServerInfo").FormatString(Count));
            this.ServerInfoTimer.Change(2000, Timeout.Infinite);
        }

        public void UpdateControls()
        {
            this.FlagIcon.Image = LanguageManager.CurrentLanguage.Flag;
            if (LanguageListContextMenuStrip.Items.Count > 0)
                LanguageListContextMenuStrip.Items.Clear();
            foreach (LanguageInfo Language in LanguageManager.Languages)
                LanguageListContextMenuStrip.Items.Add(Language.Name, Language.Flag);
            this.StartButton.Text = LanguageManager.CurrentLanguage.GetValueByName("Start");
            this.RegisterButton.Text = LanguageManager.CurrentLanguage.GetValueByName("Register");
            this.ExitButton.Text = LanguageManager.CurrentLanguage.GetValueByName("Exit");
            this.Text = "{0} - {1}: {2} | {3}".FormatString(StaticInfo.GameName, LanguageManager.CurrentLanguage.GetValueByName("Version"), StaticInfo.Version, " Z Team").Replace(",", ".");
            if (!StartClicked)
            {
                //this.CurrentGroupBox.Text = "{0}: {1}".FormatString(LanguageManager.CurrentLanguage.GetValueByName("Current"), LanguageManager.CurrentLanguage.GetValueByName("WaitingStart"));
                //this.TotalGroupBox.Text = "{0}: {1}".FormatString(LanguageManager.CurrentLanguage.GetValueByName("Total"), LanguageManager.CurrentLanguage.GetValueByName("WaitingStart"));
                CurrentLabel = "WaitingStart";
                this.StatusLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("WaitingStart");
                this.DownloadSpeedLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("DownloadSpeed").FormatString("0KB/s");
            }
            else if (Updated)
            {
                CurrentLabel = "AllFilesAreUpdated";
                this.StatusLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("AllFilesAreUpdated");
                this.TotalProgressLabel.Text = "100%";
                this.CurrentProgressLabel.Text = "100%";
                this.DownloadedDataLabel.Text = "0MB/0MB";
                this.DownloadedFilesLabel.Text = "({0}/{0})".FormatString(Files.Count);
                this.TotalProgressBar.Current = this.TotalProgressBar.Maximum;
                this.CurrentProgressBar.Current = this.CurrentProgressBar.Maximum;
                this.DownloadSpeedLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("DownloadSpeed").FormatString("0KB/s");
                this.StartButton.Enabled = true;
            }
            else
            {
                this.StartButton.Enabled = false;
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            StartClicked = true;
            this.UpdateControls();
            if (Updated)
            {
                CurrentLabel = "AllFilesAreUpdated";
                this.TotalProgressBar.Current = this.TotalProgressBar.Maximum;
                this.CurrentProgressBar.Current = this.CurrentProgressBar.Maximum;
                if (IOHelper.FileExists(StaticInfo.GameClient))
                {
                    //MakeSerialKey();
                    Process.Start(new ProcessStartInfo
                    {
                        WorkingDirectory = StaticInfo.ClientPath,
                        Arguments = StaticInfo.ClientArguments,
                        FileName = StaticInfo.GameClient,
                        UseShellExecute = true

                    }
                    );
                    if (StaticInfo.ExitAfterStart)
                        Environment.Exit(Environment.ExitCode);
                }
                else DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("ClientExecutableNotFound"));
            }
            else
                DownloadThread.Start();
        }

        #region 3RD Links Part
        private void ForumButton_Click(object sender, EventArgs e)
        {
            Process.Start(StaticInfo.Links["ForumLink"]);
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            Process.Start(StaticInfo.Links["RegisterLink"]);
        }

        private void FacebookIcon_Click(object sender, EventArgs e)
        {
            Process.Start(StaticInfo.Links["FacebookLink"]);
        }

        private void TwitterIcon_Click(object sender, EventArgs e)
        {
            Process.Start(StaticInfo.Links["TwitterLink"]);
        }

        private void TwitterIcon_MouseEnter(object sender, EventArgs e)
        {
            Image OpacitedImage = TwitterIcon.Image.SetImageOpacity(0.4f);
            TwitterIcon.Image = OpacitedImage;
        }

        private void TwitterIcon_MouseLeave(object sender, EventArgs e)
        {
            TwitterIcon.Image = ImageHelper.GetOnlineImage(StaticInfo.Images["TwitterIcon"]);
        }

        private void FacebookIcon_MouseEnter(object sender, EventArgs e)
        {
            Image OpacitedImage = FacebookIcon.Image.SetImageOpacity(0.4f);
            FacebookIcon.Image = OpacitedImage;
        }

        private void FacebookIcon_MouseLeave(object sender, EventArgs e)
        {
            FacebookIcon.Image = ImageHelper.GetOnlineImage(StaticInfo.Images["FacebookIcon"]);
        }
        #endregion

        private void DoUpdate()
        {
            lock (Locker)
            {
                try
                {
                    int TUpdateCount = Files.Count;
                    TotalProgressBar.Call<FutureProgressBar>(TPB => TPB.Maximum = Files.Count);
                    string File = Files[0];
                    CurrentLabel = "UpdateFile";
                    StatusLabel.Call<Label>(TStatusLabel => TStatusLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("UpdateFile").FormatString(IOHelper.GetFileName(File)));
                    DownloadedFilesLabel.Call<Label>(TDownloadedFilesLabel => TDownloadedFilesLabel.Text = "({0}/{1})".FormatString(
                        TotalProgressBar.Call<FutureProgressBar, double>(TPB => TPB.Current),
                        Updates));
                    string Path = IOHelper.CombinePaths(StaticInfo.ClientPath, File.Replace(StaticInfo.Links["WebLink"] + "Files/", "").Replace("\\", "/"));
                    if (!IOHelper.DirectoryExists(IOHelper.GetFileDirectory(Path)))
                        IOHelper.CreateDirectory(IOHelper.GetFileDirectory(Path));
                    Stopwatch SpeedWatch = new Stopwatch();
                    using (WebClient Work = new WebClient())
                    {
                        Work.DownloadProgressChanged += (s, e) =>
                            {
                                DownloadSpeedLabel.Call<Label>(DSL => DSL.Text = LanguageManager.CurrentLanguage.GetValueByName("DownloadSpeed").FormatString(Convert.ToInt64((Convert.ToDouble(e.BytesReceived) / SpeedWatch.Elapsed.TotalSeconds)).SizeSuffix() + "/s"));
                                CurrentProgressBar.Call<FutureProgressBar>(CPB => CPB.Current = e.ProgressPercentage);
                                DownloadedDataLabel.Call<Label>(DDL => DDL.Text = "{0}/{1}".FormatString(e.BytesReceived.SizeSuffix(), e.TotalBytesToReceive.SizeSuffix()));
                                CurrentProgressLabel.Call<Label>(CPL => CPL.Text = CurrentProgressBar.Progress.ToString() + "%");
                                TotalProgressLabel.Call<Label>(TPL => TPL.Text = TotalProgressBar.Progress.ToString() + "%");
                            };
                        Work.DownloadFileCompleted += (s, e) =>
                            {
                                if (e.Cancelled)
                                {
                                    DialogHelper.ShowMessage(LanguageManager.CurrentLanguage.GetValueByName("DownloadCanceled").Replace("[BR]", Environment.NewLine), LanguageManager.CurrentLanguage.GetValueByName("WarningMessage"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    Environment.Exit(Environment.ExitCode);
                                }
                                if (e.Error != null) DoException(e.Error);
                                SpeedWatch.Stop();
                                Files.Remove(File);
                                TotalProgressBar.Call<FutureProgressBar>(TPBar => TPBar.Current++);
                                StatusLabel.Call<Label>(SLabel => SLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("UpdateFile").FormatString(IOHelper.GetFileName(File)));
                                if (Files.Count == 0)
                                    this.Call<MainForm>(Frm => Frm.EndUpdate());
                                else
                                    this.DoUpdate();
                            };
                        SpeedWatch.Start();
                        Work.DownloadFileAsync(new Uri(File), Path);
                    }
                }
                catch (Exception ex)
                {
                    DoException(ex);
                }
            }
        }

        private void DoException(Exception ex)
        {
            if (ex is WebException)
            {
                WebException WE = (WebException)ex;
                string Message = LanguageManager.CurrentLanguage.GetValueByName("DefaultError");
                if (WE.Response != null && WE.Response is HttpWebResponse)
                {
                    HttpWebResponse Response = WE.Response as HttpWebResponse;
                    switch (Response.StatusCode)
                    {
                        case HttpStatusCode.NotFound: Message = LanguageManager.CurrentLanguage.GetValueByName("Web404").Replace("[BR]", Environment.NewLine); break;
                        case HttpStatusCode.Forbidden: Message = LanguageManager.CurrentLanguage.GetValueByName("Web403").Replace("[BR]", Environment.NewLine); break;
                        default: Message += Environment.NewLine + LanguageManager.CurrentLanguage.GetValueByName("WebException").FormatString(WE.ToString()); break;
                    }
                }
                DialogHelper.ShowError(Message);
                Environment.Exit(Environment.ExitCode);
            }
            else if (ex is IOException)
            {
                DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("IOError").Replace("[BR]", Environment.NewLine));
                Environment.Exit(Environment.ExitCode);
            }
            else
            {
                DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("DefaultException").FormatString(ex.Message + " " + ex.StackTrace).Replace("[BR]", Environment.NewLine));
                Environment.Exit(Environment.ExitCode);
            }
        }

        private void EndUpdate()
        {
            StartButton.Enabled = true;
            this.UpdateControls();
        }

        private void NewsBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            NewsBrowser.Show();
            WebBar.Hide();
            #region BackgroundImageFixed
            Dictionary<string, string> Style = CSSHelper.ToDictionary(NewsBrowser.Document.GetElementsByTagName("body")[0], true);
            if (Style.ContainsKey("background-position")) Style.Remove("background-position");
            if (Style.ContainsKey("background-size")) Style.Remove("background-size");
            if (Style.ContainsKey("background-repeat")) Style.Remove("background-repeat");
            if (Style.ContainsKey("background-attachment")) Style.Remove("background-attachment");
            if (Style.ContainsKey("height")) Style.Remove("height");
            if (Style.ContainsKey("width")) Style.Remove("width");
            if (Style.ContainsKey("position")) Style.Remove("position");
            if (Style.ContainsKey("top")) Style.Remove("top");
            if (Style.ContainsKey("left")) Style.Remove("left");
            Style.Add("background-size", "100% 100%");
            Style.Add("background-position", "center");
            NewsBrowser.Document.GetElementsByTagName("body")[0].Style = CSSHelper.ToString(Style);
            #endregion
            NewsBrowser.ScrollBarsEnabled = false;
            NewsBrowser.IsWebBrowserContextMenuEnabled = false;
        }

        private void NewsBrowser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            if (e.CurrentProgress > 0)
            {
                WebBar.Maximum = (int)e.MaximumProgress;
                WebBar.Current = (int)e.CurrentProgress;
            }
        }

        private void FlagIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (!StartButton.Enabled)
            {
                DialogHelper.ShowWarning(LanguageManager.CurrentLanguage.GetValueByName("WaitingDownloadEnd"));
                return;
            }
            if (e.Button == MouseButtons.Left)
                LanguageListContextMenuStrip.Show(this, this.FlagIcon.Location);
        }

        private void LanguageListContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            LanguageManager.SetCurrentLanguage(LanguageManager.GetLanguageByName(e.ClickedItem.Text));
            if (!LanguageManager.Save())
            {
                DialogHelper.ShowError("Error on LanguageManager.Save()!![BR]Verify String.xml permissions and restart launcher!".Replace("[BR]", Environment.NewLine));
                Environment.Exit(Environment.ExitCode);
            }
            this.UpdateControls();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogHelper.ShowMessage(LanguageManager.CurrentLanguage.GetValueByName("ExitMessage"), LanguageManager.CurrentLanguage.GetValueByName("ExitTitle"), MessageBoxButtons.YesNo) == DialogResult.Yes)
                Environment.Exit(Environment.ExitCode);
            else e.Cancel = true;
        }

        private void DownloadSpeedLabel_Click(object sender, EventArgs e)
        {

        }

        private void WebBar_Click(object sender, EventArgs e)
        {

        }

        private void DownloadedDataLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
