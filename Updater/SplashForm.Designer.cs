﻿namespace Launcher
{
    partial class SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.FlagIcon = new System.Windows.Forms.PictureBox();
            this.LanguageListContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.FlagIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Font = new System.Drawing.Font("Shruti", 13F);
            this.StatusLabel.ForeColor = System.Drawing.Color.White;
            this.StatusLabel.Location = new System.Drawing.Point(137, 156);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(163, 29);
            this.StatusLabel.TabIndex = 0;
            this.StatusLabel.Text = "Checking Version....";
            // 
            // FlagIcon
            // 
            this.FlagIcon.BackColor = System.Drawing.Color.Transparent;
            this.FlagIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FlagIcon.Location = new System.Drawing.Point(441, 169);
            this.FlagIcon.Name = "FlagIcon";
            this.FlagIcon.Size = new System.Drawing.Size(16, 16);
            this.FlagIcon.TabIndex = 1;
            this.FlagIcon.TabStop = false;
            this.FlagIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FlagIcon_MouseClick);
            // 
            // LanguageListContextMenuStrip
            // 
            this.LanguageListContextMenuStrip.Name = "LanguageListContextMenuStrip";
            this.LanguageListContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.LanguageListContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            this.LanguageListContextMenuStrip.TabStop = true;
            this.LanguageListContextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.LanguageListContextMenuStrip_ItemClicked);
            // 
            // SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Launcher.Properties.Resources.splash_logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(469, 187);
            this.Controls.Add(this.FlagIcon);
            this.Controls.Add(this.StatusLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = global::Launcher.Properties.Resources.AppIcon;
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SplashForm";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            ((System.ComponentModel.ISupportInitialize)(this.FlagIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.PictureBox FlagIcon;
        private System.Windows.Forms.ContextMenuStrip LanguageListContextMenuStrip;
    }
}