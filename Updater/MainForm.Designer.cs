﻿namespace Launcher
{
    partial class MainForm
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">verdade se for necessário descartar os recursos gerenciados; caso contrário, falso.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte do Designer - não modifique
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.FlagIcon = new System.Windows.Forms.PictureBox();
            this.LanguageListContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TotalProgressLabel = new System.Windows.Forms.Label();
            this.DownloadedFilesLabel = new System.Windows.Forms.Label();
            this.DownloadedDataLabel = new System.Windows.Forms.Label();
            this.CurrentProgressLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.FacebookIcon = new System.Windows.Forms.PictureBox();
            this.TwitterIcon = new System.Windows.Forms.PictureBox();
            this.DownloadSpeedLabel = new System.Windows.Forms.Label();
            this.ServerInfoLabel = new System.Windows.Forms.Label();
            this.CurrentProgressBar = new Bussiness.Themes.FutureProgressBar();
            this.ExitButton = new Bussiness.Themes.FutureButton();
            this.TotalProgressBar = new Bussiness.Themes.FutureProgressBar();
            this.ForumButton = new Bussiness.Themes.FutureButton();
            this.RegisterButton = new Bussiness.Themes.FutureButton();
            this.StartButton = new Bussiness.Themes.FutureButton();
            this.ButtonsBottomSeparator = new Bussiness.Themes.FutureSeperator();
            this.FormWebSeparator = new Bussiness.Themes.FutureSeperator();
            this.FormTheme = new Bussiness.Themes.FutureTheme();
            this.futureSeperator1 = new Bussiness.Themes.FutureSeperator();
            this.NewsBrowser = new System.Windows.Forms.WebBrowser();
            this.WebBar = new Bussiness.Themes.FutureProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.FlagIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FacebookIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TwitterIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // FlagIcon
            // 
            this.FlagIcon.Location = new System.Drawing.Point(701, 279);
            this.FlagIcon.Name = "FlagIcon";
            this.FlagIcon.Size = new System.Drawing.Size(16, 11);
            this.FlagIcon.TabIndex = 5;
            this.FlagIcon.TabStop = false;
            this.FlagIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FlagIcon_MouseClick);
            // 
            // LanguageListContextMenuStrip
            // 
            this.LanguageListContextMenuStrip.Name = "LanguageListContextMenuStrip";
            this.LanguageListContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            this.LanguageListContextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.LanguageListContextMenuStrip_ItemClicked);
            // 
            // TotalProgressLabel
            // 
            this.TotalProgressLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.TotalProgressLabel.Location = new System.Drawing.Point(12, 385);
            this.TotalProgressLabel.Name = "TotalProgressLabel";
            this.TotalProgressLabel.Size = new System.Drawing.Size(62, 15);
            this.TotalProgressLabel.TabIndex = 15;
            this.TotalProgressLabel.Text = "100%";
            this.TotalProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DownloadedFilesLabel
            // 
            this.DownloadedFilesLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.DownloadedFilesLabel.Location = new System.Drawing.Point(640, 385);
            this.DownloadedFilesLabel.Name = "DownloadedFilesLabel";
            this.DownloadedFilesLabel.Size = new System.Drawing.Size(78, 13);
            this.DownloadedFilesLabel.TabIndex = 16;
            this.DownloadedFilesLabel.Text = "(0/1000)";
            this.DownloadedFilesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DownloadedDataLabel
            // 
            this.DownloadedDataLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.DownloadedDataLabel.Location = new System.Drawing.Point(640, 356);
            this.DownloadedDataLabel.Name = "DownloadedDataLabel";
            this.DownloadedDataLabel.Size = new System.Drawing.Size(78, 13);
            this.DownloadedDataLabel.TabIndex = 19;
            this.DownloadedDataLabel.Text = "0KB/0KB";
            this.DownloadedDataLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DownloadedDataLabel.Click += new System.EventHandler(this.DownloadedDataLabel_Click);
            // 
            // CurrentProgressLabel
            // 
            this.CurrentProgressLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.CurrentProgressLabel.Location = new System.Drawing.Point(12, 356);
            this.CurrentProgressLabel.Name = "CurrentProgressLabel";
            this.CurrentProgressLabel.Size = new System.Drawing.Size(62, 13);
            this.CurrentProgressLabel.TabIndex = 18;
            this.CurrentProgressLabel.Text = "100%";
            this.CurrentProgressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StatusLabel
            // 
            this.StatusLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.StatusLabel.Location = new System.Drawing.Point(58, 330);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(283, 13);
            this.StatusLabel.TabIndex = 20;
            this.StatusLabel.Text = "Updating File: X";
            // 
            // FacebookIcon
            // 
            this.FacebookIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FacebookIcon.Location = new System.Drawing.Point(679, 277);
            this.FacebookIcon.Name = "FacebookIcon";
            this.FacebookIcon.Size = new System.Drawing.Size(16, 16);
            this.FacebookIcon.TabIndex = 21;
            this.FacebookIcon.TabStop = false;
            this.FacebookIcon.Click += new System.EventHandler(this.FacebookIcon_Click);
            this.FacebookIcon.MouseEnter += new System.EventHandler(this.FacebookIcon_MouseEnter);
            this.FacebookIcon.MouseLeave += new System.EventHandler(this.FacebookIcon_MouseLeave);
            // 
            // TwitterIcon
            // 
            this.TwitterIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TwitterIcon.Location = new System.Drawing.Point(652, 277);
            this.TwitterIcon.Name = "TwitterIcon";
            this.TwitterIcon.Size = new System.Drawing.Size(16, 16);
            this.TwitterIcon.TabIndex = 22;
            this.TwitterIcon.TabStop = false;
            this.TwitterIcon.Click += new System.EventHandler(this.TwitterIcon_Click);
            this.TwitterIcon.MouseEnter += new System.EventHandler(this.TwitterIcon_MouseEnter);
            this.TwitterIcon.MouseLeave += new System.EventHandler(this.TwitterIcon_MouseLeave);
            // 
            // DownloadSpeedLabel
            // 
            this.DownloadSpeedLabel.AutoEllipsis = true;
            this.DownloadSpeedLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.DownloadSpeedLabel.Location = new System.Drawing.Point(461, 330);
            this.DownloadSpeedLabel.Name = "DownloadSpeedLabel";
            this.DownloadSpeedLabel.Size = new System.Drawing.Size(182, 13);
            this.DownloadSpeedLabel.TabIndex = 23;
            this.DownloadSpeedLabel.Text = "Speed: 0KB/s";
            this.DownloadSpeedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.DownloadSpeedLabel.Click += new System.EventHandler(this.DownloadSpeedLabel_Click);
            // 
            // ServerInfoLabel
            // 
            this.ServerInfoLabel.AutoEllipsis = true;
            this.ServerInfoLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.ServerInfoLabel.Location = new System.Drawing.Point(528, 279);
            this.ServerInfoLabel.Name = "ServerInfoLabel";
            this.ServerInfoLabel.Size = new System.Drawing.Size(106, 13);
            this.ServerInfoLabel.TabIndex = 24;
            this.ServerInfoLabel.Text = "0 Players Online";
            this.ServerInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CurrentProgressBar
            // 
            this.CurrentProgressBar.Color1 = System.Drawing.Color.Maroon;
            this.CurrentProgressBar.Color2 = System.Drawing.Color.Red;
            this.CurrentProgressBar.Current = 0D;
            this.CurrentProgressBar.Location = new System.Drawing.Point(80, 352);
            this.CurrentProgressBar.Maximum = 100D;
            this.CurrentProgressBar.Name = "CurrentProgressBar";
            this.CurrentProgressBar.Progress = 0D;
            this.CurrentProgressBar.Size = new System.Drawing.Size(554, 23);
            this.CurrentProgressBar.TabIndex = 17;
            // 
            // ExitButton
            // 
            this.ExitButton.BackB1 = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.ExitButton.BackB6 = System.Drawing.Color.DimGray;
            this.ExitButton.Font = new System.Drawing.Font("Verdana", 8F);
            this.ExitButton.ForeB5 = System.Drawing.Color.Silver;
            this.ExitButton.HighLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.ExitButton.Location = new System.Drawing.Point(399, 275);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            this.ExitButton.Size = new System.Drawing.Size(123, 23);
            this.ExitButton.TabIndex = 13;
            this.ExitButton.Text = "Exit";
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // TotalProgressBar
            // 
            this.TotalProgressBar.Color1 = System.Drawing.Color.Maroon;
            this.TotalProgressBar.Color2 = System.Drawing.Color.Red;
            this.TotalProgressBar.Current = 0D;
            this.TotalProgressBar.Location = new System.Drawing.Point(80, 381);
            this.TotalProgressBar.Maximum = 100D;
            this.TotalProgressBar.Name = "TotalProgressBar";
            this.TotalProgressBar.Progress = 0D;
            this.TotalProgressBar.Size = new System.Drawing.Size(554, 23);
            this.TotalProgressBar.TabIndex = 14;
            // 
            // ForumButton
            // 
            this.ForumButton.BackB1 = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.ForumButton.BackB6 = System.Drawing.Color.DimGray;
            this.ForumButton.Font = new System.Drawing.Font("Verdana", 8F);
            this.ForumButton.ForeB5 = System.Drawing.Color.Silver;
            this.ForumButton.HighLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.ForumButton.Location = new System.Drawing.Point(270, 275);
            this.ForumButton.Name = "ForumButton";
            this.ForumButton.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            this.ForumButton.Size = new System.Drawing.Size(123, 23);
            this.ForumButton.TabIndex = 12;
            this.ForumButton.Text = "Forum";
            this.ForumButton.Click += new System.EventHandler(this.ForumButton_Click);
            // 
            // RegisterButton
            // 
            this.RegisterButton.BackB1 = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.RegisterButton.BackB6 = System.Drawing.Color.DimGray;
            this.RegisterButton.Font = new System.Drawing.Font("Verdana", 8F);
            this.RegisterButton.ForeB5 = System.Drawing.Color.Silver;
            this.RegisterButton.HighLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.RegisterButton.Location = new System.Drawing.Point(141, 275);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            this.RegisterButton.Size = new System.Drawing.Size(123, 23);
            this.RegisterButton.TabIndex = 11;
            this.RegisterButton.Text = "Register";
            this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // StartButton
            // 
            this.StartButton.BackB1 = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.StartButton.BackB6 = System.Drawing.Color.DimGray;
            this.StartButton.Font = new System.Drawing.Font("Verdana", 8F);
            this.StartButton.ForeB5 = System.Drawing.Color.Silver;
            this.StartButton.HighLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.StartButton.Location = new System.Drawing.Point(12, 275);
            this.StartButton.Name = "StartButton";
            this.StartButton.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))));
            this.StartButton.Size = new System.Drawing.Size(123, 23);
            this.StartButton.TabIndex = 9;
            this.StartButton.Text = "Start";
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // ButtonsBottomSeparator
            // 
            this.ButtonsBottomSeparator.Location = new System.Drawing.Point(12, 304);
            this.ButtonsBottomSeparator.Name = "ButtonsBottomSeparator";
            this.ButtonsBottomSeparator.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.ButtonsBottomSeparator.Size = new System.Drawing.Size(706, 23);
            this.ButtonsBottomSeparator.TabIndex = 8;
            this.ButtonsBottomSeparator.Text = "futureSeperator2";
            // 
            // FormWebSeparator
            // 
            this.FormWebSeparator.BackColor = System.Drawing.Color.Red;
            this.FormWebSeparator.Location = new System.Drawing.Point(12, 246);
            this.FormWebSeparator.Name = "FormWebSeparator";
            this.FormWebSeparator.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.FormWebSeparator.Size = new System.Drawing.Size(706, 23);
            this.FormWebSeparator.TabIndex = 1;
            // 
            // FormTheme
            // 
            this.FormTheme.BackP1 = System.Drawing.Color.Black;
            this.FormTheme.BackP2 = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.FormTheme.BackP3 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.FormTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormTheme.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            /*this.FormTheme.GradientColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(22)))), ((int)(((byte)(22)))))};*/
            this.FormTheme.Location = new System.Drawing.Point(0, 0);
            this.FormTheme.Name = "FormTheme";
            this.FormTheme.Size = new System.Drawing.Size(730, 416);
            this.FormTheme.TabIndex = 0;
            this.FormTheme.Text = "FormTheme";
            this.FormTheme.TitleColor = System.Drawing.Color.LightGray;
            // 
            // futureSeperator1
            // 
            this.futureSeperator1.BackColor = System.Drawing.Color.Red;
            this.futureSeperator1.Location = new System.Drawing.Point(12, 246);
            this.futureSeperator1.Name = "futureSeperator1";
            this.futureSeperator1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.futureSeperator1.Size = new System.Drawing.Size(706, 23);
            this.futureSeperator1.TabIndex = 1;
            // 
            // NewsBrowser
            // 
            this.NewsBrowser.IsWebBrowserContextMenuEnabled = false;
            this.NewsBrowser.Location = new System.Drawing.Point(12, 48);
            this.NewsBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.NewsBrowser.Name = "NewsBrowser";
            this.NewsBrowser.ScriptErrorsSuppressed = true;
            this.NewsBrowser.ScrollBarsEnabled = false;
            this.NewsBrowser.Size = new System.Drawing.Size(706, 192);
            this.NewsBrowser.TabIndex = 4;
            this.NewsBrowser.Visible = false;
            this.NewsBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.NewsBrowser_DocumentCompleted);
            this.NewsBrowser.ProgressChanged += new System.Windows.Forms.WebBrowserProgressChangedEventHandler(this.NewsBrowser_ProgressChanged);
            // 
            // WebBar
            // 
            this.WebBar.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.WebBar.Color2 = System.Drawing.Color.Maroon;
            this.WebBar.Current = 0D;
            this.WebBar.Location = new System.Drawing.Point(21, 136);
            this.WebBar.Maximum = 200D;
            this.WebBar.Name = "WebBar";
            this.WebBar.Progress = 0D;
            this.WebBar.Size = new System.Drawing.Size(686, 23);
            this.WebBar.TabIndex = 7;
            this.WebBar.Click += new System.EventHandler(this.WebBar_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 416);
            this.Controls.Add(this.ServerInfoLabel);
            this.Controls.Add(this.DownloadSpeedLabel);
            this.Controls.Add(this.TwitterIcon);
            this.Controls.Add(this.FacebookIcon);
            this.Controls.Add(this.CurrentProgressBar);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.CurrentProgressLabel);
            this.Controls.Add(this.DownloadedDataLabel);
            this.Controls.Add(this.DownloadedFilesLabel);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.TotalProgressLabel);
            this.Controls.Add(this.TotalProgressBar);
            this.Controls.Add(this.ForumButton);
            this.Controls.Add(this.RegisterButton);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.ButtonsBottomSeparator);
            this.Controls.Add(this.WebBar);
            this.Controls.Add(this.NewsBrowser);
            this.Controls.Add(this.FlagIcon);
            this.Controls.Add(this.FormWebSeparator);
            this.Controls.Add(this.FormTheme);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WarRock Download Manager";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.FlagIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FacebookIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TwitterIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bussiness.Themes.FutureTheme FormTheme;
        private Bussiness.Themes.FutureSeperator FormWebSeparator;
        private System.Windows.Forms.PictureBox FlagIcon;
        private Bussiness.Themes.FutureSeperator futureSeperator1;
        private Bussiness.Themes.FutureSeperator ButtonsBottomSeparator;
        private Bussiness.Themes.FutureButton StartButton;
        private System.Windows.Forms.ContextMenuStrip LanguageListContextMenuStrip;
        private Bussiness.Themes.FutureButton RegisterButton;
        private Bussiness.Themes.FutureButton ForumButton;
        private Bussiness.Themes.FutureButton ExitButton;
        private Bussiness.Themes.FutureProgressBar TotalProgressBar;
        private System.Windows.Forms.Label TotalProgressLabel;
        private System.Windows.Forms.Label DownloadedFilesLabel;
        private System.Windows.Forms.Label DownloadedDataLabel;
        private System.Windows.Forms.Label CurrentProgressLabel;
        private Bussiness.Themes.FutureProgressBar CurrentProgressBar;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.PictureBox FacebookIcon;
        private System.Windows.Forms.PictureBox TwitterIcon;
        private System.Windows.Forms.Label DownloadSpeedLabel;
        private System.Windows.Forms.Label ServerInfoLabel;
        private System.Windows.Forms.WebBrowser NewsBrowser;
        private Bussiness.Themes.FutureProgressBar WebBar;
    }
}

