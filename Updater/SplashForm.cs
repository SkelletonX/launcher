﻿using Bussiness.Helper;
using Bussiness.Info;
using Bussiness.Manager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Launcher
{
    public partial class SplashForm : Form
    {
        private Thread SplashThread;
        private object Locker = new object();
        public SplashForm()
        {
            if (!LanguageManager.Init())
            {
                DialogHelper.ShowMessage("Error on LanguageManager!![BR]Verify String.xml and restart launcher!".Replace("[BR]", Environment.NewLine));
                Environment.Exit(Environment.ExitCode);
            }
            if (StaticInfo.OnlyOneGameInstance)
                if (ProcessHelper.ProcessExists(IOHelper.GetFileNameWithoutExtension(StaticInfo.GameClient)))
                {
                    DialogHelper.ShowWarning(LanguageManager.CurrentLanguage.GetValueByName("GameIsRunning").Replace("[BR]",Environment.NewLine));
                    Environment.Exit(Environment.ExitCode);
                }
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            InitializeComponent();
            this.FlagIcon.Cursor = Cursors.Hand;
            Dictionary<string,string> Settings = SettingsHelper.LoadSettings();
            this.Region = Region.FromHrgn(NativeFunctions.CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            this.Cursor = Cursors.WaitCursor;
            this.StatusLabel.Font = new Font(Settings["SplashFont"], this.StatusLabel.Font.Size, FontStyle.Regular);
            foreach(LanguageInfo Language in LanguageManager.Languages)
                LanguageListContextMenuStrip.Items.Add(Language.Name, Language.Flag);
            this.FlagIcon.ContextMenuStrip = LanguageListContextMenuStrip;
            this.UpdateControls();
            this.Text = StaticInfo.GameName;
            this.ShowInTaskbar = false;
        }

        public void UpdateControls()
        {
            this.StatusLabel.Text = LanguageManager.CurrentLanguage.GetValueByName("CheckingStatus");
            this.FlagIcon.Image = LanguageManager.CurrentLanguage.Flag;
        }

        protected override void OnLoad(EventArgs e)
        {
            SplashThread = new Thread(SplashConn);
            SplashThread.SetApartmentState(ApartmentState.STA);
            SplashThread.Start();
            base.OnLoad(e);
        }

        [STAThread]
        private void SplashConn()
        {
            lock (Locker)
            {
                if (!WebHelper.IsOnline(StaticInfo.ServerEndPoint))
                {
                    DialogHelper.ShowWarning(LanguageManager.CurrentLanguage.GetValueByName("UnderMaintenance"), LanguageManager.CurrentLanguage.GetValueByName("StatusChecker"));
                    Environment.Exit(Environment.ExitCode);
                }
                this.StatusLabel.Call<Label>(SL => SL.Text = LanguageManager.CurrentLanguage.GetValueByName("CheckingVersion"));
                byte[] XMLData = new byte[0];
                try
                {
                    using (WebClient Work = new WebClient())
                    {
                        Work.Encoding = Encoding.UTF8;
                        XMLData = Work.DownloadData(StaticInfo.Links["WebLink"] + "Version.xml");
                    }
                }
                catch (WebException WE)
                {
                    string Message = LanguageManager.CurrentLanguage.GetValueByName("DefaultError");
                    if (WE.Response != null && WE.Response is HttpWebResponse)
                    {
                        HttpWebResponse Response = WE.Response as HttpWebResponse;
                        switch (Response.StatusCode)
                        {
                            case HttpStatusCode.NotFound: Message = LanguageManager.CurrentLanguage.GetValueByName("Web404").Replace("[BR]", Environment.NewLine); break;
                            case HttpStatusCode.Forbidden: Message = LanguageManager.CurrentLanguage.GetValueByName("Web403").Replace("[BR]", Environment.NewLine); break;
                            default: Message += Environment.NewLine + LanguageManager.CurrentLanguage.GetValueByName("WebException").FormatString(WE.ToString()); break;
                        }
                    }
                    DialogHelper.ShowError(Message);
                    Environment.Exit(Environment.ExitCode);
                }
                catch (IOException)
                {
                    DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("IOError").Replace("[BR]", Environment.NewLine));
                    Environment.Exit(Environment.ExitCode);
                }
                catch (Exception E)
                {
                    DialogHelper.ShowError(LanguageManager.CurrentLanguage.GetValueByName("DefaultException").FormatString(E.Message + " " + E.StackTrace).Replace("[BR]", Environment.NewLine));
                    Environment.Exit(Environment.ExitCode);
                }
                finally
                {
                    StatusLabel.Call<Label>(SL => SL.Text = LanguageManager.CurrentLanguage.GetValueByName("CheckingFiles"));
                    byte[] DecodedXMLData = XMLData.DecryptData();
                    string FixedData = Encoding.UTF8.GetString(DecodedXMLData).Trim().Trim(new char[] { '\uFEFF', '\u200B' });//BOM FIX
                    XmlDocument DOC = new XmlDocument();
                    DOC.LoadXml(FixedData);
                    List<string> TempFiles = new List<string>();
                    foreach (XmlNode Node in DOC.SelectSingleNode("/*"))
                    {
                        string File = IOHelper.CombinePaths(StaticInfo.ClientPath, Node.InnerText);
                        if (!IOHelper.FileExists(File) || Node.Attributes["MD5"] != null && HashHelper.GetFileMD5(File) != Node.Attributes["MD5"].Value)
                            TempFiles.Add(StaticInfo.Links["WebLink"] + "Files/" + Node.InnerText);
                    }
                    Double Version = DOC.DocumentElement.Attributes["Version"].Value.To<double>();
                    if (Version > StaticInfo.Version)
                    {
                        if (DialogHelper.ShowMessage(LanguageManager.CurrentLanguage.GetValueByName("NewVersionDetected").Replace("[BR]", Environment.NewLine), "Version Checker", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            Process.Start((IOHelper.CombinePaths(Environment.CurrentDirectory, StaticInfo.LauncherFiles["Downloader"])), "/v:{0}".FormatString(Version.To<string>().Replace(",", "-")));
                        Environment.Exit(Environment.ExitCode);
                    }
                    else
                        EndCheck(TempFiles);
                    //SplashThread.Abort();
                }
            }
        }

        public void EndCheck(List<string> Files)
        {
            if (InvokeRequired)
                Invoke(new Action(() => EndCheck(Files)));
            else
            {
                MainForm Main = new MainForm(Files);
                Main.Show();
                this.Hide();
            }
        }

        private void FlagIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                LanguageListContextMenuStrip.Show(this, this.FlagIcon.Location);
        }

        private void LanguageListContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            LanguageManager.SetCurrentLanguage(LanguageManager.GetLanguageByName(e.ClickedItem.Text));
            if (!LanguageManager.Save())
            {
                DialogHelper.ShowError("Error on LanguageManager.Save()!![BR]Verify String.xml permissions and restart launcher!".Replace("[BR]", Environment.NewLine));
                Environment.Exit(Environment.ExitCode);
            }
            this.UpdateControls();
        }
    }
}
